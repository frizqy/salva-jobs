<?php

return [
    'user' => [
        'sysadmin' => [
            'password' => env('SYSADMIN_PWD')
        ]
    ],
];
