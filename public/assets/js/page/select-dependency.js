var selectJobSpecialization = $('#select-job-specialization')
var selectJobRole = $('#select-job-role')

function onChangeJobSpecialization() {
    var valueJobSpecialization = selectJobSpecialization.val()
    var url = selectJobSpecialization.attr('data-route')

    selectJobRole.empty();
    selectJobRole.attr('disabled', true);
    selectJobRole.append('<option value=""> Loading... </option>');

    $.ajax({
        url: url + '?job_specialization_id=' + valueJobSpecialization,
        type: "GET",
        dataType: "json",
        success: function (data) {
            var dataStringify = JSON.stringify(data);
            var dataParse = JSON.parse(dataStringify);

            console.log()
            if (dataParse.data.length > 0) {
                selectJobRole.empty();
                selectJobRole.attr('disabled', false);

                selectJobRole.append('<option value="">-- Select --</option>');

                $.each(dataParse.data, function (key, item) {
                    selectJobRole.append('<option value="' + item.id + '">' + item.label + '</option>');
                });
            } else {
                    selectJobRole.empty();
                    selectJobRole.append('<option value="" hidden>No data for this Division</option>');
                    selectJobRole.attr('disabled', true);
            }
        },
        error: function (xhr, status, error) {
            // 
        }
    });
}