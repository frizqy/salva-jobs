handleClick();

function handleClick() {
    isChecked = $('#chkbox-sallary').is(":checked")
    var minSallary = $('#hide-input-min-sallary').val()
    var maxSallary = $('#hide-input-max-sallary').val()
    if (isChecked) {
        $('#input-min-sallary').val(0);
        $('#input-max-sallary').val(0);

        $('#input-min-sallary').attr('readonly', true);
        $('#input-max-sallary').attr('readonly', true);
    } else {
        $('#input-min-sallary').val(minSallary);
        $('#input-max-sallary').val(maxSallary);

        formatCurrencyInput('input-min-sallary');
        formatCurrencyInput('input-max-sallary');

        $('#input-min-sallary').attr('readonly', false);
        $('#input-max-sallary').attr('readonly', false);
    }
}

function formatCurrencyInput(id) {
    var angka = $('#' + id).val()

    var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
    }

    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    $('#' + id).val(rupiah)
}

"use strict";
(function ($) {
    "use strict";
    $('#datepicker-open-until').datepicker({ dateFormat: 'yyyy-mm-dd' })
})(jQuery);


// Default ckeditor
CKEDITOR.replace('txtarea-job_description', {
    on: {
        contentDom: function (evt) {
            // Allow custom context menu only with table elemnts.
            evt.editor.editable().on('contextmenu', function (contextEvent) {
                var path = evt.editor.elementPath();

                if (!path.contains('table')) {
                    contextEvent.cancel();
                }
            }, null, null, 5);
        }
    }
});

CKEDITOR.replace('txtarea-requirement', {
    on: {
        contentDom: function (evt) {
            // Allow custom context menu only with table elemnts.
            evt.editor.editable().on('contextmenu', function (contextEvent) {
                var path = evt.editor.elementPath();

                if (!path.contains('table')) {
                    contextEvent.cancel();
                }
            }, null, null, 5);
        }
    }
});

CKEDITOR.replace('txtarea-benefit', {
    on: {
        contentDom: function (evt) {
            // Allow custom context menu only with table elemnts.
            evt.editor.editable().on('contextmenu', function (contextEvent) {
                var path = evt.editor.elementPath();

                if (!path.contains('table')) {
                    contextEvent.cancel();
                }
            }, null, null, 5);
        }
    }
});

CKEDITOR.replace('txtarea-qualification', {
    on: {
        contentDom: function (evt) {
            // Allow custom context menu only with table elemnts.
            evt.editor.editable().on('contextmenu', function (contextEvent) {
                var path = evt.editor.elementPath();

                if (!path.contains('table')) {
                    contextEvent.cancel();
                }
            }, null, null, 5);
        }
    }
});