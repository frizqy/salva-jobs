(function ($) {
    "use strict";

    var repeaterEducation = $('.repeater-educations').repeater({

        initEmpty: true,

        defaultValues: {
            'text-input': 'foo'
        },

        show: function () {
            $(this).slideDown();

            $(this).find('.date-ym').datepicker({ dateFormat: 'yyyy-mm' })

            $(this).find('.date-year').datepicker({
                changeMonth: false,
                changeYear: true,
                showButtonPanel: true,
                yearRange: '2000:2050', // Optional Year Range
                dateFormat: 'yyyy'
            });

            $(this).find('.js-example-basic-multiple-no-tags').select2({});
            $(this).find('.js-example-basic-single').select2({});

        },

        hide: function (deleteElement) {
            if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        },

        ready: function (setIndexes) {
        }
    })

    var initEducation = $('#selected-education').val();
    if(initEducation != undefined){
        repeaterEducation.setList(JSON.parse(initEducation));
    }

})(jQuery);