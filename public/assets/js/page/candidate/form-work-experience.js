(function ($) {
    "use strict";

    var repeaterWorkExperience = $('.repeater-work-experiences').repeater({

        initEmpty: true,

        defaultValues: {
            'text-input': 'foo'
        },

        show: function () {
            $(this).slideDown();

            $(this).find('.date-ym').datepicker({ dateFormat: 'yyyy-mm' })

            $(this).find('.date-year').datepicker({
                changeMonth: false,
                changeYear: true,
                showButtonPanel: true,
                yearRange: '2000:2050', // Optional Year Range
                dateFormat: 'yyyy'
            });

            $(this).find('.js-example-basic-multiple-no-tags').select2({});
            $(this).find('.js-example-basic-single').select2({});

            var formControlName = $(this).find('textarea.form-control').attr('name')
            $(this).find('textarea.form-control').attr('id', formControlName);

            CKEDITOR.replace(formControlName, {
                on: {
                    contentDom: function (evt) {
                        // Allow custom context menu only with table elemnts.
                        evt.editor.editable().on('contextmenu', function (contextEvent) {
                            var path = evt.editor.elementPath();

                            if (!path.contains('table')) {
                                contextEvent.cancel();
                            }
                        }, null, null, 5);
                    }
                }
            });

            var checkboxName = $(this).find('.chkbox-present').attr('name')
            var findIndexes = formControlName.match(/[[0-9]*]/g)
            var index = findIndexes[0].replace('\[', '')
            index = index.replace('\]', '')
            console.log(index);

            $(this).find('.chkbox-present').attr('id', 'checkbox_present_' + index);
            $(this).find('.input-date-to').attr('id', 'input_date_to_' + index);

            $('#checkbox_present_' + index).change(function () {
                if (this.checked) {
                    $('#input_date_to_' + index).prop('readonly', true)
                    $('#input_date_to_' + index).val('present')
                } else {
                    $('#input_date_to_' + index).prop('readonly', false)
                    $('#input_date_to_' + index).val('')
                }
            });
        },

        hide: function (deleteElement) {
            if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        },

        ready: function (setIndexes) {
        }
    })

    var initValueWorkExperience = $('#selected-work-experience').val();
    if (initValueWorkExperience != undefined) {
        repeaterWorkExperience.setList(JSON.parse(initValueWorkExperience));
    }

})(jQuery);