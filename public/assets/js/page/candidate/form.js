handleClick();

function handleClick() {
    formatCurrencyInput('input-current-sallary');
    formatCurrencyInput('input-expected-sallary');
}

function formatCurrencyInput(id) {
    var angka = $('#' + id).val()

    var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
    }

    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    $('#' + id).val(rupiah)
}

"use strict";
(function ($) {
    "use strict";
    $('#datepicker-birthday').datepicker({ dateFormat: 'yyyy-mm-dd' })
    $('.date-ym').datepicker({ dateFormat: 'yyyy-mm' })
    $('.date-year').datepicker({
        changeMonth: false,
        changeYear: true,
        showButtonPanel: true,
        yearRange: '2000:2050', // Optional Year Range
        dateFormat: 'yyyy'
    });

})(jQuery);