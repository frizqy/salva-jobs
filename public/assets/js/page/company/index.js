$(document).ready(function () {

    var dtTable = $('#table-company')
    var dtUrl = dtTable.attr('data-action')

    var pageLength = 10
    var lengthMenu = [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]]
    var autoWidth = false
    var buttons = ['excel']
    var responsive = true
    var processing = true
    var serverSide = true
    var destroy = true

    dtTable.DataTable({
        pageLength: pageLength,
        lengthMenu: lengthMenu,
        autoWidth: autoWidth,
        buttons: buttons,
        destroy: destroy,
        responsive: responsive,
        ajax: {
            url: dtUrl
        },
        processing: processing,
        serverSide: serverSide,
        columns: [
            {
                data: 'id', orderable: false,
                mRender: function (data, type, obj, meta) {
                    var idx = meta.row + meta.settings._iDisplayStart + 1;
                    return idx;
                }
            },
            {
                data: 'name'
            },
            {
                data: 'address'
            },
            {
                data: 'image',
                mRender: function (data, type, obj, meta) {
                    if (data != '') {
                        var html = '<center><img class="img-thumbnail" src="/' + data + '" style="object-fit: cover; width:100px !important; height:100px !important;" itemprop="thumbnail" alt="Image description"></center>';
                        return html
                    }

                    return ''
                }
            },
            {
                data: 'description'
            },
            {
                data: 'id',
                mRender: function (data, type, obj, meta) {
                    var html ='';
                    html += '<center>'

                    html += '<a href="/companies/' + data + '">'
                    html += '<i class="fa fa-fw fa-eye"></i>'
                    html += '</a>'

                    html += '&nbsp;&nbsp;'

                    html += '<a href="/companies/' + data + '/edit">'
                    html += '<i class="fa fa-fw fa-pencil"></i>'
                    html += '</a>'

                    html += '</center>'


                    return html
                }
            }
        ]
    });
});