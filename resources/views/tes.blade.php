<!DOCTYPE html>
<html>

<head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        div.main {
            position: relative;
            width: 100%;
        }

        div.left-side {
            position: absolute;
            left: 0;
            height: 100%;
            background-color: #222528;
            width: 250px;
        }

        div.right-side {
            position: relative;
            top: 0;
            left: 250px;
            width: 495px;
        }

        img.rounded-circle {
            border: 1px solid #dddddd;
            height: 150px !important;
            width: 150px !important;
            object-fit: cover;
        }

        .card {
            margin: auto;
            background-color: #222528;
            color: white;
            border-radius: 0;
        }

        .card-right {
            margin: 50px 10px;
            background-color: white;
            color: #222528;
            border-radius: 0;
        }

        .media {
            text-align: center;
        }

        .separator {
            width: 100%;
            font-weight: bold;
            border-bottom: 1px #dddddd;
        }

        p.label {
            margin-bottom: none !important;
            font-weight: bold;
            padding: 0px !important;
        }

        p.info {
            margin-top: 0 !important;
            margin-bottom: 2px !important;
            padding: 0px !important;
        }

        hr.custom {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgb(255 255 255);
        }
    </style>
</head>

<body>
    <div class="main">
        <div class="left-side">

            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="profile-title">
                            <div class="media">
                                <img class="rounded-circle" src="/{{ $candidate->photo_profile }}">
                            </div>
                        </div>
                    </div>

                    {{--
                    <div style="margin: 24px 0;">
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </div>
                    --}}

                    <hr class="custom" />
                    <p class="label">Email</p>
                    <p class="info">{{ $candidate->email }}</p>

                    <p class="label">Phone Number</p>
                    <p class="info">{{ $candidate->phone_number }}</p>

                    <p class="label">Gender</p>
                    <p class="info">{{ $candidate->gender }}</p>

                    <p class="label">Birthday</p>
                    <p class="info">{{ $candidate->birthday_location }}, {{ $candidate->birthday }}</p>

                    <hr class="custom" />
                    <p class="label">Work Preference</p>
                    <p class="info"> @isset($candidate->work_preference) {{ $candidate->work_preference_label }} @endisset</p>

                    @if($candidate->current_sallary != 0 && $candidate->expected_sallary != 0)
                    <p class="label">Current Sallary</p>
                    <p class="info"> {{ number_format($candidate->current_sallary,0,",",".") }}</p>

                    <p class="label">Expected Sallary</p>
                    <p class="info"> {{ number_format($candidate->expected_sallary,0,",",".") }}</p>
                    @endif

                    <p class="label">Notice Period</p>
                    <p class="info"> {{ $candidate->notice_period }}</p>

                    <p class="label">Reason to Leave</p>
                    <p class="info"> {{ $candidate->reason_to_leave }}</p>
                </div>
            </div>
        </div>

        <div class="right-side">

            <div class="card-right">
                <h2 class="name">{{ $candidate->name }}</h2>
                @if($candidate->job_specialization != null && $candidate->job_role != null) {{ $candidate->job_specialization->label }} - {{ $candidate->job_role->label }} @endif

                <br />
                <br />

                <h4><small><b>Work Experiences</b></small></h4>
                @if(sizeof($candidate->work_experiences) == 0)
                N/A
                @endif
                @foreach($candidate->work_experiences as $work_experience)
                <small>{{ $work_experience->date_from }} - {{ $work_experience->date_to }}</small>
                <p>{{ $work_experience->company }} - {{ $work_experience->job_title }}</p>
                <p>{!! $work_experience->job_description !!}</p>
                {!! $work_experience->used_technology !!}
                <hr />
                @endforeach

                <h4><small><b>Education</b></small></h4>
                @if(sizeof($candidate->educations) == 0)
                N/A
                @endif
                @foreach($candidate->educations as $education)
                <small>{{ $education->date_from }} - {{ $education->date_to }}</small>
                <p class="info">{{ $education->degree }} - {{ $education->specialization }}</p>
                <p class="info"><small>GPA {{ $education->gpa }} of 4</small></p>
                <p class="info">{{ $education->name }}</p>
                <hr />
                @endforeach

                <h4><small><b>Certification</b></small></h4>
                @if(sizeof($candidate->certifications) == 0)
                N/A
                @endif
                @foreach($candidate->certifications as $certification)
                <small>{{ $certification->year }}</small>
                <p>{{ $certification->name }} - {{ $certification->organization }}</p>
                <p>{{ $certification->event_description }}</p>
                <hr />
                @endforeach

                <h4><small><b>Languages</b></small></h4>
                @if(sizeof($candidate->languages) == 0)
                N/A
                @endif
                @foreach($candidate->languages as $language)
                <b>{{ $language->language }}</b>
                <br/>Writing: {{ $language->writing }}
                <br/>Speaking: {{ $language->speaking }}
                <br/>Listening: {{ $language->listening }}
                <hr/>
                @endforeach
            </div>
        </div>
    </div>
</body>

</html>