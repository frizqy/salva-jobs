<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts/assets/meta')

    <title>Jobs - Salva</title>

    @include('layouts/assets/base-css')
</head>

<body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="theme-loader">
            <div class="loader-p"></div>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <section>
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-12">
                    <div class="login-card">
                        @yield('auth-content')
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('layouts/assets/base-js')
</body>

</html>