@extends('auth/base')

@section('auth-content')

@include('layouts/flashmessage')

<form class="theme-form login-form" method="POST" action="{{ route('register-employer') }}">
    @csrf
    <h4>{{ __('Register Employer') }}</h4>
    <br />
    <div class="form-group">
        <label>{{ __('Name') }}</label>
        <div class="small-group">
            <div class="input-group"><span class="input-group-text"><i class="icon-user"></i></span>
                <input class="form-control" type="text" required="" placeholder="Fist Name" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
            </div>
            @error('first_name')
            <div style="color:red;">{{ $message }}</div>
            @enderror
            <div class="input-group"><span class="input-group-text"><i class="icon-user"></i></span>
                <input class="form-control" type="text" required="" placeholder="Last Name" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name">
            </div>
            @error('last_name')
            <div style="color:red;">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <label>{{ __('Business Name') }}</label>
        <div class="input-group"><span class="input-group-text"><i class="icon-bag"></i></span>
            <input class="form-control" type="text" required="" placeholder="Business / Company Name" name="company_name" value="{{ old('company_name') }}" required autocomplete="company_name">
        </div>
        @error('company_name')
        <div style="color:red;">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label>{{ __('Business Address') }}</label>
        <div class="input-group"><span class="input-group-text"><i class="icon-location-pin"></i></span>
            <textarea class="form-control @error('company_address') is-invalid @enderror" rows="3" name="company_address">{{ old('company_address') }}</textarea>
        </div>
        @error('company_address')
        <div style="color:red;">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label>{{ __('Email Address') }}</label>
        <div class="input-group"><span class="input-group-text"><i class="icon-email"></i></span>
            <input class="form-control" type="email" required="" placeholder="test@mail.com" name="email" value="{{ old('email') }}" required autocomplete="email">
        </div>
        @error('email')
        <div style="color:red;">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label>{{ __('Password') }}</label>
        <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
            <input class="form-control" type="password" placeholder="*********" name="password" required>
            <div class="show-hide"><span class="show"> </span></div>
        </div>
        @error('password')
        <div style="color:red;">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label>{{ __('Confirm Password') }}</label>
        <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
            <input class="form-control" type="password" placeholder="*********" name="password_confirmation" required>
            <div class="show-hide"><span class="show"> </span></div>
        </div>
        @error('password_confirmation')
        <div style="color:red;">{{ $message }}</div>
        @enderror
    </div>

    <br />
    <div class="form-group">
        <button class="btn btn-primary btn-block" type="submit">{{ __('Register') }}</button>
    </div>

    <p>Already have an account?<a class="ms-2" href="{{ route('login') }}">Sign In</a></p>
</form>

@endsection