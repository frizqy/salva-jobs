<!-- Page Sidebar Start-->
<header class="main-nav">
    @guest
    <div class="sidebar-user text-center">
    </div>
    @else
    <div class="sidebar-user text-center">
        <img class="img-90 rounded-circle" style="height:90px !important" alt="" src="/{{ Auth::user()->photo_profile }}">
        <a href="user-profile.html">
            <h6 class="mt-3 f-14 f-w-600">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h6>
        </a>
        {{-- <p class="mb-0 font-roboto">Human Resources Department</p> --}}
    </div>
    @endguest
    <nav>
        <div class="main-navbar">
            <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                    <li class="back-btn">
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li><a class="nav-link menu-title link-nav" href="/"><i data-feather="search"></i><span>Job Search</span></a></li>
                    @guest
                    <li><a class="nav-link menu-title link-nav" href="{{ route('login') }}"><i data-feather="log-in"></i><span>Sign In</span></a></li>
                    @else

                    @role('company_admin')
                    <li><a class="nav-link menu-title link-nav" id="companies" href="{{ route('admin-company-detail', ['id' => Auth::user()->company_id]) }}"><i data-feather="home"></i><span>Company Profile</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="jobs" href="{{ route('admin-job-index') }}"><i data-feather="briefcase"></i><span>Job Post</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="search-cvs" href="{{ route('admin-search-cv-index') }}"><i data-feather="search"></i><span>Search CV</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="co-workers" href="#"><i data-feather="users"></i><span>Co - Worker</span></a></li>

                    <li class="sidebar-main-title">
                        <div>
                            <h6>Profile</h6>
                        </div>
                    </li>
                    <li><a class="nav-link menu-title link-nav" id="profile" href="{{ route('profile') }}"><i data-feather="user"></i><span>Edit Account</span></a></li>
                    @endrole

                    @role('super_admin')
                    <li><a class="nav-link menu-title link-nav" id="jobs" href="{{ route('admin-job-index') }}"><i data-feather="briefcase"></i><span>Job</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="candidates" href="{{ route('admin-candidate-index') }}"><i data-feather="home"></i><span>Candidates</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="companies" href="{{ route('admin-company-index') }}"><i data-feather="home"></i><span>Company</span></a></li>

                    <li class="sidebar-main-title">
                        <div>
                            <h6>Master</h6>
                        </div>
                    </li>
                    <li><a class="nav-link menu-title link-nav" id="job-types" href="{{ route('admin-job-type-index') }}"><i data-feather="archive"></i><span>Job Types</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="degrees" href="{{ route('admin-degree-index') }}"><i data-feather="award"></i><span>Degree</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="job-specializations" href="{{ route('admin-job-specialization-index') }}"><i data-feather="package"></i><span>Job Specialization</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="job-roles" href="{{ route('admin-job-role-index') }}"><i data-feather="pocket"></i><span>Job Role</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="work-preferences" href="{{ route('admin-work-preference-index') }}"><i data-feather="server"></i><span>Work Preference</span></a></li>
                    <li><a class="nav-link menu-title link-nav" id="skills" href="{{ route('admin-skill-index') }}"><i data-feather="tag"></i><span>Skill</span></a></li>
                    @endrole

                    @endguest
                </ul>
            </div>
            {{-- <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div> --}}
        </div>
    </nav>
</header>
<!-- Page Sidebar Ends-->