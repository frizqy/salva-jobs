<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts/assets/meta')

    <title>{{ $title }} - Salva</title>

    @include('layouts/assets/base-css')
    @yield('content-css')
</head>

<body class="dark-sidebar">
    <!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="theme-loader">
            <div class="loader-p"></div>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper" id="pageWrapper">

        @include('layouts/header')

        <!-- Page Body Start-->
        <div class="page-body-wrapper horizontal-menu starter-kit-fix">
            @include('layouts/sidebar')

            <div class="page-body">
                <input type="hidden" id="content-wrapper-orientation" value="@isset($layout_wrapper) {{ $layout_wrapper }} @else compact-wrapper @endisset" />
                @if($breadcrumbs_holder)
                @include('layouts/breadcrumbs')
                @endif

                @include('layouts/flashmessage')

                @yield('content')
            </div>

            @include('layouts/footer')

        </div>
    </div>

    @include('layouts/assets/base-js')
    @yield('content-js')
</body>

</html>