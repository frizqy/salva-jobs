@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0">
                    <h5>Company</h5>
                </div>
                @if($action == 'edit')
                <form class="form theme-form" method="POST" action="{{ route('admin-job-update', ['id' => $job->id]) }}">
                    @else
                    <form class="form theme-form" method="POST" action="{{ route('admin-job-store') }}">
                        @endif
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Company *</label>
                                        <select class="js-example-basic-single col-sm-12 @error('company_id') is-invalid @enderror" name="company_id">
                                            @foreach($companies as $company)
                                            <option @if(old('company_id', (isset($job)) ? $job->company_id : '') == $company->id) selected @endif value="{{ $company->id }}">{{ $company->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('company_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Job Type *</label>
                                        <select class="js-example-basic-single col-sm-12 @error('job_type_id') is-invalid @enderror" name="job_type_id">
                                            @foreach($job_types as $job_type)
                                            <option @if(old('job_type_id', (isset($job)) ? $job->job_type_id : '') == $job_type->id) selected @endif value="{{ $job_type->id }}">{{ $job_type->label }}</option>
                                            @endforeach
                                        </select>
                                        @error('job_type_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Title *</label>
                                        <input class="form-control @error('title') is-invalid @enderror" type="text" name="title" value="{{ old('title', (isset($job)) ? $job->title : '') }}">
                                        @error('title')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Year Experience *</label>
                                        <select class="js-example-basic-single col-sm-12 @error('year_experience') is-invalid @enderror" name="year_experience">
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 0) selected @endif value="0">0 Year</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 1) selected @endif value="1">1 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 2) selected @endif value="2">2 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 3) selected @endif value="3">3 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 4) selected @endif value="4">4 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 5) selected @endif value="5">5 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 6) selected @endif value="6">6 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 7) selected @endif value="7">7 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 8) selected @endif value="8">8 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 9) selected @endif value="9">9 Years</option>
                                            <option @if(old('year_experience', (isset($job)) ? $job->year_experience : '') == 10) selected @endif value="10">10 Years</option>
                                        </select>
                                        @error('year_experience')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Career Level *</label>
                                        <select class="js-example-basic-single col-sm-12 @error('career_level_id') is-invalid @enderror" name="career_level_id">
                                            @foreach($career_levels as $career_level)
                                            <option @if(old('career_level_id', (isset($job)) ? $job->career_level_id : '') == $career_level->id) selected @endif value="{{ $career_level->id }}">{{ $career_level->label }}</option>
                                                @endforeach
                                        </select>
                                        @error('career_level_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            {{--
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Small Description *</label>
                                        <textarea class="form-control @error('description') is-invalid @enderror" rows="5" name="description">{{ old('description', (isset($job)) ? $job->description : '') }}</textarea>
                                        @error('description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div> 
                            --}}

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Job Specialization *</label>
                                        <select id="select-job-specialization" onchange="onChangeJobSpecialization()" data-route="{{ route('dt-job-roles') }}" class="js-example-basic-single col-sm-12 @error('job_specialization_id') is-invalid @enderror" name="job_specialization_id">
                                            <option value="">-- Select --</option>
                                            @foreach($job_specializations as $job_specialization)
                                            <option @if(in_array($job_specialization->id, $selected_job_specializations)) selected @endif value="{{ $job_specialization->id }}">{{ $job_specialization->label }}</option>
                                            @endforeach
                                        </select>
                                        @error('job_specialization_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Job Role *</label>
                                        <select id="select-job-role" class="js-example-basic-single col-sm-12 @error('job_role_id') is-invalid @enderror" name="job_role_id">
                                            <option value="">-- Select --</option>
                                            @foreach($job_roles as $job_role)
                                            <option @if(in_array($job_role->id, $selected_job_roles)) selected @endif value="{{ $job_role->id }}">{{ $job_role->id }}</option>
                                            @endforeach
                                        </select>
                                        @error('job_role')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Skill *</label>
                                        <select class="js-example-basic-multiple-no-tags col-sm-12 @error('skill') is-invalid @enderror" multiple="multiple" name="skill[]">
                                            @foreach($skills as $skill)
                                            <option @if(in_array($skill->label, $selected_skills)) selected @endif value="{{ $skill->label }}">{{ $skill->label }}</option>
                                            @endforeach
                                        </select>
                                        @error('skill')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Work Preferences *</label>
                                        <select class="js-example-basic-multiple-no-tags col-sm-12 @error('work_preference') is-invalid @enderror" multiple="multiple" name="work_preference[]">
                                            @foreach($work_preferences as $work_preference)
                                            <option @if(in_array($work_preference->label, $selected_work_preferences)) selected @endif value="{{ $work_preference->label }}">{{ $work_preference->label }}</option>
                                            @endforeach
                                        </select>
                                        @error('work_preference')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Work Location *</label>
                                        <select class="js-example-basic-multiple col-sm-12 @error('work_location') is-invalid @enderror" multiple="multiple" name="work_location[]">
                                            @foreach($work_locations as $work_location)
                                            <option  @if(in_array($work_location->label, $selected_work_locations)) selected @endif value="{{ $work_location->label }}">{{ $work_location->label }}</option>
                                            @endforeach
                                        </select>
                                        @error('work_location')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Job Description</label>
                                        <textarea class="@error('job_description') is-invalid @enderror" id="txtarea-job_description" name="job_description" cols="30" rows="10">
                                    {!! old('job_description', (isset($job)) ? $job->job_description : '') !!}
                                    </textarea>
                                        @error('job_description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Requirement</label>
                                        <textarea class="@error('requirement') is-invalid @enderror" id="txtarea-requirement" name="requirement" cols="30" rows="10">
                                    {!! old('requirement', (isset($job)) ? $job->requirement : '') !!}
                                    </textarea>
                                        @error('requirement')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Benefit</label>
                                        <textarea class="@error('benefit') is-invalid @enderror" id="txtarea-benefit" name="benefit" cols="30" rows="10">
                                    {!! old('benefit', (isset($job)) ? $job->benefit : '') !!}
                                    </textarea>
                                        @error('benefit')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Qualification</label>
                                        <textarea class="@error('qualification') is-invalid @enderror" id="txtarea-qualification" name="qualification" cols="30" rows="10">
                                    {!! old('qualification', (isset($job)) ? $job->qualification : '') !!}
                                    </textarea>
                                        @error('qualification')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3">
                                    <div class="col-xl-6 xl-100">
                                        <div class="form-group">
                                            <label class="form-label">Sallary</label>

                                            <label class="d-block" for="chkbox-sallary">
                                                @php
                                                $checked = '';
                                                if($action == 'edit'){
                                                    if($job->min_sallary == 0 && $job->max_sallary == 0){
                                                        $checked = 'checked';
                                                    }
                                                }
                                                @endphp
                                                <input {{ $checked }} class="checkbox_animated" id="chkbox-sallary" type="checkbox" onclick="handleClick()"> Is Confidential?
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 xl-100 xl-mt-job">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="form-label">Min</label>
                                                <div class="input-group">
                                                    <input type="hidden" id="hide-input-min-sallary" value="{{ old('min_sallary', (isset($job)) ? $job->min_sallary : '') }}"/>
                                                    <input oninput="formatCurrencyInput('input-min-sallary');" id="input-min-sallary" class="form-control digits @error('min_sallary') is-invalid @enderror" type="text" name="min_sallary" value="{{ old('min_sallary', (isset($job)) ? $job->min_sallary : '') }}" placeholder="Min Sallary">
                                                    @error('min_sallary')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="form-label">Max</label>
                                                <div class="input-group">
                                                    <input type="hidden" id="hide-input-max-sallary" value="{{ old('max_sallary', (isset($job)) ? $job->max_sallary : '') }}"/>
                                                    <input oninput="formatCurrencyInput('input-max-sallary');" id="input-max-sallary" class="form-control digits @error('max_sallary') is-invalid @enderror" type="text" name="max_sallary" value="{{ old('max_sallary', (isset($job)) ? $job->max_sallary : '') }}" placeholder="Max Sallary">
                                                    @error('max_sallary')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Open Until *</label>
                                        <input id="datepicker-open-until" class="form-control digits @error('open_until') is-invalid @enderror" type="text" name="open_until" data-language="en" value="{{ old('open_until', (isset($job)) ? $job->open_until : '') }}">
                                        @error('open_until')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <a href="{{ route('admin-company-index') }}" class="btn btn-light">Cancel</a>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
<!-- Container-fluid Ends-->

@endsection

@section('content-js')
<script src="{{ asset('assets/js/page/job/form.js') }}"></script>
<script src="{{ asset('assets/js/page/select-dependency.js') }}"></script>
@endsection