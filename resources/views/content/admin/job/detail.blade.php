@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0">
                    <h5>Job Detail</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Company</label>
                                <input class="form-control" type="text" value="{{ $job->company->name }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Job Type</label>
                                <input class="form-control" type="text" value="{{ $job->job_type->label }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Title</label>
                                <input class="form-control" type="text" value="{{ $job->title }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Year Experience</label>
                                <input class="form-control" type="text" value="{{ $job->year_experience }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Career Level</label>
                                <input class="form-control" type="text" value="{{ $job->career_level->label }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Job Specification</label>
                                <input class="form-control" type="text" value="{{ $job->job_specialization }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Work Location</label>
                                <input class="form-control" type="text" value="{{ $job->work_location }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Job Description</label>
                                <br />
                                {!! $job->job_description !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Requirement</label>
                                <br />
                                {!! $job->requirement !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Benefit</label>
                                <br />
                                {!! $job->benefit !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Qualification</label>
                                <br />
                                {!! $job->qualification !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="mb-3">
                            <div class="col-xl-12 xl-100 xl-mt-job">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="form-label">Min Sallary</label>
                                        <div class="input-group">
                                            <input id="input-min-sallary" readonly class="form-control digits" type="text" value="{{ $job->min_sallary }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="form-label">Max Sallary</label>
                                        <div class="input-group">
                                            <input id="input-max-sallary" readonly class="form-control digits" type="text" value="{{ $job->max_sallary }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Open Until</label>
                                <input class="form-control" type="text" value="{{ $job->open_until }}" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-end">
                    <a href="{{ route('admin-job-edit', ['id' => $job->id]) }}" class="btn btn-primary">Edit</a>
                    <a href="{{ route('admin-job-index') }}" class="btn btn-light">Back</a>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- Container-fluid Ends-->

@endsection