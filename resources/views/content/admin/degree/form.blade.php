@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0">
                    <h5>Degree</h5>
                </div>
                @if($action == 'edit')
                <form class="form theme-form" method="POST" action="{{ route('admin-degree-update', ['id' => $degree->id]) }}" enctype="multipart/form-data">
                    @else
                    <form class="form theme-form" method="POST" action="{{ route('admin-degree-store') }}" enctype="multipart/form-data">
                        @endif
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Code</label>
                                        <input readonly class="form-control @error('code') is-invalid @enderror" type="text" name="code" value="{{ old('code', (isset($degree)) ? $degree->code : '') }}">
                                        @error('code')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Degree*</label>
                                        <input class="form-control @error('label') is-invalid @enderror" type="text" name="label" value="{{ old('label', (isset($degree)) ? $degree->label : '') }}">
                                        @error('label')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <a href="{{ route('admin-degree-index') }}" class="btn btn-light">Cancel</a>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
<!-- Container-fluid Ends-->

@endsection