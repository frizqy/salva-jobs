@extends('layouts/base')

@section('content')
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <!-- Zero Configuration  Starts-->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0">
                    <h5>Degree</h5>
                </div>
                <div class="card-body btn-showcase pb-0 text-end">
                    <a class="btn btn-primary" type="button" href="{{ route('admin-degree-add') }}">Add New</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="display" id="table-job-degree" data-action="{{ route('admin-degree-index') }}">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Zero Configuration  Ends-->
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection

@section('content-js')
<script src="{{ asset('assets/js/page/degree/index.js') }}"></script>
@endsection