@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0">
                    <h5>Job Role</h5>
                </div>
                @if($action == 'edit')
                <form class="form theme-form" method="POST" action="{{ route('admin-job-role-update', ['id' => $jobRole->id]) }}" enctype="multipart/form-data">
                    @else
                    <form class="form theme-form" method="POST" action="{{ route('admin-job-role-store') }}" enctype="multipart/form-data">
                        @endif
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Code</label>
                                        <input readonly class="form-control @error('code') is-invalid @enderror" type="text" name="code" value="{{ old('code', (isset($jobRole)) ? $jobRole->code : '') }}">
                                        @error('code')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Job Specialization*</label>
                                        <select class="js-example-basic-single col-sm-12 @error('job_specialization_id') is-invalid @enderror" name="job_specialization_id">
                                            @foreach($job_spzcializations as $job_specialization)
                                            <option @if(old('job_specialization_id', (isset($jobRole)) ? $jobRole->job_specialization_id : '') == $job_specialization->id) selected @endif value="{{ $job_specialization->id }}">{{ $job_specialization->label }}</option>
                                            @endforeach
                                        </select>
                                        @error('job_specialization_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Job Role*</label>
                                        <input class="form-control @error('label') is-invalid @enderror" type="text" name="label" value="{{ old('label', (isset($jobRole)) ? $jobRole->label : '') }}">
                                        @error('label')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer text-end">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <a href="{{ route('admin-job-role-index') }}" class="btn btn-light">Cancel</a>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
<!-- Container-fluid Ends-->

@endsection