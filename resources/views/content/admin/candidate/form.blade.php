@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0">
                    <h5>Candidate</h5>
                </div>
                @if($action == 'edit')
                <form class="form theme-form" id="form-candidate" method="POST" action="{{ route('admin-candidate-update', ['id' => $candidate->id]) }}" enctype="multipart/form-data">
                    @else
                    <form class="form theme-form" id="form-candidate" method="POST" action="{{ route('admin-candidate-store') }}" enctype="multipart/form-data">
                        @endif
                        <div class="card-body">
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        @if(isset($candidate) && $candidate->photo_profile != '')
                                        <img class="img-90 rounded-circle" style="border: 1px solid #dddddd; height:90px !important; object-fit: cover;" src="/{{ $candidate->photo_profile }}" alt="{{ $candidate->name }}" title="{{ $candidate->name }}">
                                        <br />
                                        <br />
                                        @endisset
                                        <label class="form-label">Candidate Photo Profile</label>
                                        <input class="form-control @error('photo_profile') is-invalid @enderror" type="file" name="photo_profile" accept=".jpg,.jpeg,.png">
                                        @error('photo_profile')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">CV</label>
                                        <input class="form-control @error('cv') is-invalid @enderror" type="file" name="cv">
                                        @error('cv')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Name *</label>
                                        <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name', (isset($candidate)) ? $candidate->name : '') }}">
                                        @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Email *</label>
                                        <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" value="{{ old('email', (isset($candidate)) ? $candidate->email : '') }}">
                                        @error('email')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Phone Number *</label>
                                        <input class="form-control @error('phone_number') is-invalid @enderror" type="text" name="phone_number" value="{{ old('phone_number', (isset($candidate)) ? $candidate->phone_number : '') }}">
                                        @error('phone_number')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3">
                                    <div class="col-xl-12 xl-100 xl-mt-job">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="form-label">Birthday</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="form-label">Location</label>
                                                <div class="input-group">
                                                    <input class="form-control @error('birthday_location') is-invalid @enderror" type="text" name="birthday_location" value="{{ old('birthday_location', (isset($candidate)) ? $candidate->birthday_location : '') }}">
                                                    @error('birthday_location')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="form-label">Date</label>
                                                <div class="input-group">
                                                    <input id="datepicker-birthday" class="form-control digits @error('birthday') is-invalid @enderror" type="text" name="birthday" data-language="en" value="{{ old('birthday', (isset($candidate)) ? $candidate->birthday : '') }}">
                                                    @error('birthday')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Work Preference *</label>
                                        <select class="js-example-basic-multiple-no-tags col-sm-12 @error('work_preference_id') is-invalid @enderror" name="work_preference_id[]" multiple="multiple">
                                            @foreach($work_preferences as $work_preference)
                                            <option @if(in_array($work_preference->id, old('work_preference_id', (isset($selected_work_preference) ? $selected_work_preference : []) ))) selected @endif value="{{ $work_preference->id }}">{{ $work_preference->label }}</option>
                                            @endforeach
                                        </select>
                                        @error('work_preference_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Gender *</label>
                                        <select class="js-example-basic-single col-sm-12 @error('gender') is-invalid @enderror" name="gender">
                                            <option value="">-- Select --</option>
                                            <option @if(old('gender', (isset($candidate)) ? $candidate->gender : '') == 'male') selected @endif value="male">Male</option>
                                            <option @if(old('gender', (isset($candidate)) ? $candidate->gender : '') == 'female') selected @endif value="female">Female</option>
                                        </select>
                                        @error('gender')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3">
                                    <div class="col-xl-12 xl-100 xl-mt-job">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="form-label">Job Specialization</label>
                                                <div class="input-group">
                                                    <select id="select-job-specialization" onchange="onChangeJobSpecialization()" data-route="{{ route('dt-job-roles') }}" class="js-example-basic-single col-sm-12 @error('job_specialization_id') is-invalid @enderror" name="job_specialization_id">
                                                        <option value="">-- Select --</option>
                                                        @foreach($job_specializations as $job_specialization)
                                                        <option @if(old('job_specialization_id', (isset($candidate)) ? $candidate->job_specialization_id : '') == $job_specialization->id) selected @endif value="{{ $job_specialization->id }}">{{ $job_specialization->label }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('job_specialization_id')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <label class="form-label">Job Role</label>
                                                <div class="input-group">
                                                    <select id="select-job-role" class="js-example-basic-single col-sm-12 @error('job_role_id') is-invalid @enderror" name="job_role_id">
                                                        <option value="">-- Select --</option>
                                                        @foreach($job_roles as $job_role)
                                                        <option @if(old('job_role_id', (isset($candidate)) ? $candidate->job_role_id : '') == $job_role->id) selected @endif value="{{ $job_role->id }}">{{ $job_role->label }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('job_role')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3">
                                    <div class="col-xl-12 xl-100 xl-mt-job">
                                        <div class="row">
                                            <div class="col-2">
                                                <label class="form-label">Current Sallary (IDR)</label>
                                                <div class="input-group">
                                                    <select class="js-example-basic-single col-sm-12 @error('current_sallary_type') is-invalid @enderror" name="current_sallary_type">
                                                        <option value="gross">Gross</option>
                                                        <option value="nett">Nett</option>
                                                    </select>
                                                    @error('current_sallary_type')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-label">&nbsp;</label>
                                                <div class="input-group">
                                                    <input type="hidden" id="hide-input-current-sallary" value="{{ old('current_sallary', (isset($candidate)) ? $candidate->current_sallary : '') }}" />
                                                    <input oninput="formatCurrencyInput('input-current-sallary');" id="input-current-sallary" class="form-control digits @error('current_sallary') is-invalid @enderror" type="text" name="current_sallary" value="{{ old('current_sallary', (isset($candidate)) ? $candidate->current_sallary : '') }}">
                                                    @error('current_sallary')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <label class="form-label">Expected Sallary (IDR)</label>
                                                <div class="input-group">
                                                    <select class="js-example-basic-single col-sm-12 @error('expected_sallary_type') is-invalid @enderror" name="expected_sallary_type">
                                                        <option value="gross">Gross</option>
                                                        <option value="nett">Nett</option>
                                                    </select>
                                                    @error('expected_sallary_type')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <label class="form-label">&nbsp;</label>
                                                <div class="input-group">
                                                    <input type="hidden" id="hide-input-expected-sallary" value="{{ old('expected_sallary', (isset($candidate)) ? $candidate->expected_sallary : '') }}" />
                                                    <input oninput="formatCurrencyInput('input-expected-sallary');" id="input-expected-sallary" class="form-control digits @error('expected_sallary') is-invalid @enderror" type="text" name="expected_sallary" value="{{ old('expected_sallary', (isset($candidate)) ? $candidate->expected_sallary : '') }}">
                                                    @error('expected_sallary')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Reason to Leave *</label>
                                        <textarea class="form-control @error('reason_to_leave') is-invalid @enderror" rows="4" name="reason_to_leave">{{ old('reason_to_leave', (isset($candidate)) ? $candidate->reason_to_leave : '') }}</textarea>
                                        @error('reason_to_leave')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Notice Period *</label>
                                        <select class="js-example-basic-single col-sm-12 @error('notice_period') is-invalid @enderror" name="notice_period">
                                            <option value="">-- Select --</option>
                                            <option @if(old('notice_period', (isset($candidate)) ? $candidate->notice_period : '') == 'none') selected @endif value="none">None</option>
                                            <option @if(old('notice_period', (isset($candidate)) ? $candidate->notice_period : '') == 'asap') selected @endif value="asap">ASAP</option>
                                            <option @if(old('notice_period', (isset($candidate)) ? $candidate->notice_period : '') == '2 weeks') selected @endif value="2 weeks">2 weeks</option>
                                            <option @if(old('notice_period', (isset($candidate)) ? $candidate->notice_period : '') == '3 weeks') selected @endif value="3 week">3 weeks</option>
                                            <option @if(old('notice_period', (isset($candidate)) ? $candidate->notice_period : '') == '1 month') selected @endif value="1 month">1 Month</option>
                                            <option @if(old('notice_period', (isset($candidate)) ? $candidate->notice_period : '') == '2 months') selected @endif value="2 months">2 Months</option>
                                            <option @if(old('notice_period', (isset($candidate)) ? $candidate->notice_period : '') == 'more than 2 months') selected @endif value="more than 2 months">More than 2 Months</option>
                                        </select>
                                        @error('notice_period')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3">
                                    <div class="col-xl-12 xl-100 xl-mt-job">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="form-label">Social Media</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="form-label">Instagram</label>
                                                <div class="input-group">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i data-feather="instagram"></i></span>
                                                    <input class="form-control @error('instagram') is-invalid @enderror" type="text" name="instagram" value="{{ old('instagram', (isset($candidate)) ? $candidate->instagram : '') }}" placeholder="Instagram">
                                                    @error('instagram')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-label">Facebook</label>
                                                <div class="input-group">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i data-feather="facebook"></i></span>
                                                    <input class="form-control @error('facebook') is-invalid @enderror" type="text" name="facebook" value="{{ old('facebook', (isset($candidate)) ? $candidate->facebook : '') }}" placeholder="Facebook">
                                                    @error('facebook')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-label">LinkedIn</label>
                                                <div class="input-group">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i data-feather="linkedin"></i></span>
                                                    <input class="form-control @error('linkedin') is-invalid @enderror" type="text" name="linkedin" value="{{ old('linkedin', (isset($candidate)) ? $candidate->linkedin : '') }}" placeholder="Linkedin">
                                                    @error('linkedin')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Top Skill</label>
                                        <select class="js-example-basic-multiple-no-tags col-sm-12 @error('skill') is-invalid @enderror" name="skill[]" multiple="multiple">
                                            @foreach($skills as $skill)
                                            <option value="{{ $skill->id }}" @if(in_array($skill->id, old('skill', (isset($selected_skills) ? $selected_skills : []) ))) selected @endif >{{ $skill->label }}</option>
                                            @endforeach
                                        </select>
                                        @error('skill')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <br />
                                        <label class="form-label" for="chkbox-is-completed">Ready to Publish? <input class="checkbox_animated" id="chkbox-is-completed" @if(old('is_completed', (isset($candidate) ? $candidate->is_completed : '') )) checked @endif type="checkbox" name="is_completed"></label>
                                        @error('is_completed')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            @include('content.admin.candidate.form-work-experience')

                            @include('content.admin.candidate.form-education')

                            @include('content.admin.candidate.form-certification')

                            @include('content.admin.candidate.form-language')

                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                <a href="{{ route('admin-candidate-index') }}" class="btn btn-light">Cancel</a>
                            </div>
                        </div>
                    </form>
            </div>

            <div class="card">
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection

@section('content-js')
<script src="{{ asset('assets/js/repeater/repeater.js') }}"></script>
<script src="{{ asset('assets/js/page/candidate/form.js') }}"></script>
<script src="{{ asset('assets/js/page/candidate/form-education.js') }}"></script>
<script src="{{ asset('assets/js/page/candidate/form-work-experience.js') }}"></script>
<script src="{{ asset('assets/js/page/candidate/form-certification.js') }}"></script>
<script src="{{ asset('assets/js/page/candidate/form-language.js') }}"></script>
<script src="{{ asset('assets/js/page/select-dependency.js') }}"></script>
@endsection