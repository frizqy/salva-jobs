@if(isset($candidate))
<input type="hidden" id="selected-language" value="{{ json_encode($candidate->languages) }}">
@endif
<br />
<hr />
<h5>Language</h5>
<div class='repeater-language'>
    <div data-repeater-list="languages">
        <div data-repeater-item>
            <hr />
            <input type="hidden" name="id" value="" />
            <div class="row mb-3">
                <div class="col-sm-3">
                    <label class="form-label">Language</label>
                    <div class="input-group">
                        <input class="form-control @error('language') is-invalid @enderror" type="text" name="language" value="{{ old('language') }}" placeholder="">
                        @error('language')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-3">
                    <label class="form-label">Writing</label>
                    <div class="input-group">
                        <select class="js-example-basic-single col-sm-12 @error('writing') is-invalid @enderror" name="writing">
                            <option value="beginner">Beginner</option>
                            <option value="intermediate">Intermediate</option>
                            <option value="advance">Advance</option>
                        </select>
                        @error('writing')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-3">
                    <label class="form-label">Speaking</label>
                    <div class="input-group">
                        <select class="js-example-basic-single col-sm-12 @error('speaking') is-invalid @enderror" name="speaking">
                            <option value="beginner">Beginner</option>
                            <option value="intermediate">Intermediate</option>
                            <option value="advance">Advance</option>
                        </select>
                        @error('speaking')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-3">
                    <label class="form-label">Listening</label>
                    <div class="input-group">
                        <select class="js-example-basic-single col-sm-12 @error('listening') is-invalid @enderror" name="listening">
                            <option value="beginner">Beginner</option>
                            <option value="intermediate">Intermediate</option>
                            <option value="advance">Advance</option>
                        </select>
                        @error('listening')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <button class="btn btn-danger" data-repeater-delete type="button">Delete</button>
        </div>
    </div>
    <br />
    <button class="btn btn-success" data-repeater-create type="button">+ Add</button>
</div>
<br /><br />