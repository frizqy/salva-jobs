@if(isset($candidate))
<input type="hidden" id="selected-certification" value="{{ json_encode($candidate->certifications) }}">
@endif
<br />
<hr />
<h5>Certification / Course</h5>
<div class='repeater-certification'>
    <div data-repeater-list="certifications">
        <div data-repeater-item>
            <hr />
            <input type="hidden" name="id" value="" />
            <div class="row mb-3">
                <div class="col-sm-4">
                    <label class="form-label">Held by (Organization)</label>
                    <div class="input-group">
                        <input class="form-control @error('organization') is-invalid @enderror" type="text" name="organization" value="{{ old('organization') }}" placeholder="">
                        @error('organization')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Event Name</label>
                    <div class="input-group">
                        <input class="form-control @error('event_name') is-invalid @enderror" type="text" name="event_name" value="{{ old('event_name') }}" placeholder="">
                        @error('event_name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Year</label>
                    <div class="input-group">
                        <input class="form-control date-year digits @error('event_year') is-invalid @enderror" type="text" name="event_year" data-language="en" value="{{ old('event_year') }}" data-min-view="years" data-view="years" data-date-format="yyyy">
                        @error('event_year')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-sm-12">
                    <label class="form-label">Event Description</label>
                    <div class="input-group">
                        <textarea class="form-control @error('event_description') is-invalid @enderror" rows="2" name="event_description">{{ old('event_description') }}</textarea>
                        @error('event_description')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>

            <button class="btn btn-danger" data-repeater-delete type="button">Delete</button>
        </div>
    </div>
    <br />
    <button class="btn btn-success" data-repeater-create type="button">+ Add</button>
</div>
<br /><br />