@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="edit-profile">
        <div class="row">
            <div class="col-xl-4">
                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">Candidate Profile</h4>
                        <div class="card-options">
                            <a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                            <a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="profile-title">
                                <div class="media">
                                    <img class="img-90 rounded-circle" style="border: 1px solid #dddddd; height:90px !important; object-fit: cover;" src="/{{ $candidate->photo_profile }}" alt="{{ $candidate->name }}" title="{{ $candidate->name }}">
                                    <div class="media-body">
                                        <h3 class="mb-1 f-20 txt-primary">{{ ucwords($candidate->name) }}</h3>
                                        <p class="f-12">@if($candidate->job_specialization != null && $candidate->job_role != null) {{ strtoupper($candidate->job_specialization->label) }} - {{ strtoupper($candidate->job_role->label) }} @endif</p>
                                    </div>
                                </div>
                                
                                <br/>
                                <center>
                                <a href="{{ route('admin-candidate-pdf', ['id' => $candidate->id]) }}" target="_blank">Download CV</a>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <a href="{{ route('admin-candidate-invitation-email', ['id' => $candidate->id]) }}" target="_blank">Send Email</a>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <a href="https://api.whatsapp.com/send/?phone={{$candidate->phone_number}}" target="_blank">Whatsapp Me</a>
                                </center>

                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label"><b>Email-Address</b></label>
                            <br />
                            {{ $candidate->email }}
                        </div>

                        <div class="mb-3">
                            <label class="form-label"><b>Phone Number</b></label>
                            <br />
                            {{ $candidate->phone_number }}
                        </div>

                        <div class="mb-3">
                            <label class="form-label"><b>Gender</b></label>
                            <br />
                            {{ $candidate->gender }}
                        </div>

                        <div class="mb-3">
                            <label class="form-label"><b>Birthday</b></label>
                            <br />
                            {{ $candidate->birthday_location }}, {{ $candidate->birthday }}
                        </div>

                        <!-- <div class="mb-3">
                                <a href="#">Download Candidate CV</a>
                            </div> -->

                        <div class="mb-3">
                            <label class="form-label"><b>Instagram</b></label>
                            <br />
                            {{ $candidate->instagram }}
                        </div>

                        <div class="mb-3">
                            <label class="form-label"><b>Facebook</b></label>
                            <br />
                            {{ $candidate->facebook }}
                        </div>

                        <div class="mb-3">
                            <label class="form-label"><b>Linked In</b></label>
                            <br />
                            {{ $candidate->linkedin }}
                        </div>

                        <hr />


                        <div class="mb-3">
                            <label class="form-label"><b>Work Preference</b></label>
                            <br />
                            @isset($candidate->work_preference) {{ $candidate->work_preference_label }} @endisset
                        </div>

                        @if($candidate->current_sallary != 0 && $candidate->expected_sallary != 0)
                        <div class="mb-3">
                            <label class="form-label"><b>Current Sallary</b></label>
                            <br />
                            {{ number_format($candidate->current_sallary,0,",",".") }}
                        </div>

                        <div class="mb-3">
                            <label class="form-label"><b>Expected Sallary</b></label>
                            <br />
                            {{ number_format($candidate->expected_sallary,0,",",".") }}
                        </div>
                        @endif

                        <div class="mb-3">
                            <label class="form-label"><b>Notice Period</b></label>
                            <br />
                            {{ $candidate->notice_period }}
                        </div>

                        <div class="mb-3">
                            <label class="form-label"><b>Reason to Leave</b></label>
                            <br />
                            {{ $candidate->reason_to_leave }}
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">Top Skill</h4>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 mb-3">
                                @if(sizeof($candidate->skills) > 0) @foreach($candidate->skills as $skills) <button class="btn btn-primary mb-1" type="button" data-bs-original-title="" title="">{{ $skills->skill->label }}</button> @endforeach @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">Work Experiences</h4>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        @if(sizeof($candidate->work_experiences) == 0)
                        N/A
                        @endif
                        @foreach($candidate->work_experiences as $work_experience)
                        <div class="row">
                            <div class="col-6 mb-3">
                                <label class="form-label"><b>Company</b></label>
                                <br/>
                                {{ $work_experience->company }}
                            </div>

                            <div class="col-6 mb-3">
                                <label class="form-label"><b>Job Title</b></label>
                                <br/>
                                {{ $work_experience->job_title }}
                            </div>

                            <div class="col-6 mb-3">
                                <label class="form-label"><b>From</b></label>
                                <br/>
                                {{ $work_experience->date_from }}
                            </div>

                            <div class="col-6 mb-3">
                                <label class="form-label"><b>To</b></label>
                                <br/>
                                {{ $work_experience->date_to }}
                            </div>

                            <div class="col-12">
                                <label class="form-label"><b>Job Description</b></label>
                                <br/>
                                {!! $work_experience->job_description !!}
                            </div>

                            <div class="col-12">
                                <label class="form-label"><b>Tools / Skills</b></label>
                                <br/>
                                {!! $work_experience->used_technology !!}
                            </div>
                        </div>
                        <hr />
                        <br />
                        @endforeach
                    </div>
                </div>

                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">Educations</h4>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        @if(sizeof($candidate->educations) == 0)
                        N/A
                        @endif
                        @foreach($candidate->educations as $education)
                        <div class="row">
                            <div class="col-4">
                                <label class="form-label"><b>Name</b></label>
                                <br/>
                                {{ $education->name }}
                            </div>

                            <div class="col-4">
                                <label class="form-label"><b>Degree</b></label>
                                <br/>
                                {{ $education->degree }}
                            </div>

                            <div class="col-4">
                                <label class="form-label"><b>Major</b></label>
                                <br/>
                                {{ $education->specialization }}
                            </div>

                            <div class="col-4">
                                <label class="form-label"><b>From</b></label>
                                <br/>
                                {{ $education->date_from }}
                            </div>

                            <div class="col-4">
                                <label class="form-label"><b>To</b></label>
                                <br/>
                                {{ $education->date_to }}
                            </div>

                            <div class="col-4">
                                <label class="form-label"><b>GPA</b></label>
                                <br/>
                                {{ $education->gpa }}
                            </div>
                        </div>
                        <hr />
                        <br />
                        @endforeach
                    </div>
                </div>

                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">Certification</h4>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        @if(sizeof($candidate->certifications) == 0)
                        N/A
                        @endif
                        @foreach($candidate->certifications as $certification)
                        <div class="row">
                            <div class="col-3 mb-3">
                                <label class="form-label"><b>Name</b></label>
                                <br/>
                                {{ $certification->name }}
                            </div>

                            <div class="col-3 mb-3">
                                <label class="form-label"><b>Organization</b></label>
                                <br/>
                                {{ $certification->organization }}
                            </div> 

                            <div class="col-3 mb-3">
                                <label class="form-label"><b>Year</b></label>
                                <br/>
                                {{ $certification->year }}
                            </div> 

                            <div class="col-3 mb-3">
                                <label class="form-label"><b>Event Description</b></label>
                                <br/>
                                {{ $certification->event_description }}
                            </div> 
                        </div>
                        <hr />
                        <br />
                        @endforeach
                    </div>
                </div>

                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">Languages</h4>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        @if(sizeof($candidate->languages) == 0)
                        N/A
                        @else
                        <div class="row">
                            <div class="col-3">
                                <label class="form-label"><b>Language</b></label>
                            </div>

                            <div class="col-3">
                                <label class="form-label"><b>Writing</b></label>
                            </div> 

                            <div class="col-3">
                                <label class="form-label"><b>Speaking</b></label>
                            </div> 

                            <div class="col-3">
                                <label class="form-label"><b>Listening</b></label>
                            </div> 
                        </div>

                        @endif
                        @foreach($candidate->languages as $language)
                        <div class="row">
                            <div class="col-3">
                                {{ $language->language }}
                            </div>

                            <div class="col-3">
                                {{ $language->writing }}
                            </div> 

                            <div class="col-3">
                                {{ $language->speaking }}
                            </div> 

                            <div class="col-3">
                                {{ $language->listening }}
                            </div> 
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->

@endsection