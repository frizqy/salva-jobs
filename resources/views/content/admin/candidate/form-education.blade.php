@if(isset($candidate))
<input type="hidden" id="selected-education" value="{{ json_encode($candidate->educations) }}">
@endif
<br />
<hr />
<h5>Education</h5>
<div class='repeater-educations'>
    <!-- Make sure the repeater list value is different from the first repeater  -->
    <div data-repeater-list="educations">
        <div data-repeater-item>
            <hr />
            <input type="hidden" name="id" value="" /> 
            <div class="row mb-3">
                <div class="col-sm-4">
                    <label class="form-label">Name</label>
                    <div class="input-group">
                        <input class="form-control @error('education_name') is-invalid @enderror" type="text" name="education_name" value="{{ old('education_name') }}">
                        @error('education_name')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Date From</label>
                    <div class="input-group">
                        <input class="form-control date-ym digits @error('date_from') is-invalid @enderror" type="text" name="date_from" data-language="en" value="{{ old('date_from') }}" data-min-view="months" data-view="months" data-date-format="MM yyyy">
                        @error('date_from')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Date To</label>
                    <div class="input-group">
                        <input class="form-control date-ym digits @error('date_to') is-invalid @enderror" type="text" name="date_to" data-language="en" value="{{ old('date_to') }}" data-min-view="months" data-view="months" data-date-format="MM yyyy">
                        @error('date_to')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-sm-4">
                    <label class="form-label">Degree</label>
                    <div class="input-group">
                        <select class="form-select digits" id="exampleFormControlSelect9" name="education_degree">
                            <option value="">-- Select --</option>
                            <option value="high_school">SMA/SMK</option>
                            <option value="D3">D3</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                        </select>
                        @error('degree')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form-label">Major</label>
                    <div class="input-group">
                        <input class="form-control @error('specialization') is-invalid @enderror" type="text" name="specialization" value="{{ old('specialization') }}">
                        @error('specialization')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form-label">GPA</label>
                    <div class="input-group">
                        <input class="form-control @error('gpa') is-invalid @enderror" type="number" step=".1"  name="gpa" data-language="en" value="{{ old('gpa') }}">
                        @error('gpa')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>

            <button class="btn btn-danger" data-repeater-delete type="button">Delete</button>
        </div>
    </div>
    <br />
    <button class="btn btn-success" data-repeater-create type="button">+ Add</button>
</div>
<br /><br />