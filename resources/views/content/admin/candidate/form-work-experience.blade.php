@if(isset($candidate))
<input type="text" id="selected-work-experience" value="{{ json_encode($candidate->work_experiences) }}">
@endif
<br /><br />
<hr />
<h5>Work Experience</h5>
<div class='repeater-work-experiences'>
    <div data-repeater-list="work_experiences">
        <div data-repeater-item>
            <hr />
            <input type="hidden" name="id" value="" />
            <div class="row mb-3">
                <div class="col-sm-6">
                    <label class="form-label">Company</label>
                    <div class="input-group">
                        <input class="form-control @error('company') is-invalid @enderror" type="text" name="company" value="{{ old('company') }}" placeholder="">
                        @error('company')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-sm-6">
                    <label class="form-label">Job Title</label>
                    <div class="input-group">
                        <input class="form-control @error('job_title') is-invalid @enderror" type="text" name="job_title" value="{{ old('job_title') }}" placeholder="">
                        @error('job_title')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row mb-3">

                <div class="col-sm-5">
                    <label class="form-label">From</label>
                    <div class="input-group">
                        <input class="form-control date-ym digits @error('date_from') is-invalid @enderror" type="text" name="date_from" data-language="en" value="{{ old('date_from') }}" data-min-view="months" data-view="months" data-date-format="MM yyyy">
                        @error('date_from')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-sm-1">
                    <label class="form-label">To</label>
                    <div class="input-group">
                        <label class="form-label">Present <input class="checkbox_animated chkbox-present" type="checkbox" name="present" value="present"></label>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <label class="form-label">&nbsp;</label>
                    <div class="input-group">
                        <input class="form-control input-date-to date-ym digits @error('date_to') is-invalid @enderror" type="text" name="date_to" data-language="en" value="{{ old('date_to') }}" data-min-view="months" data-view="months" data-date-format="MM yyyy">
                        @error('date_to')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-sm-12">
                    <label class="form-label">Job Description</label>
                    <div class="input-group">
                        <textarea id="txt-area-job-desc" class="form-control @error('job_description') is-invalid @enderror" rows="2" name="job_description">{{ old('job_description') }}</textarea>
                        @error('job_description')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-sm-12">
                    <label class="form-label">Tools / SKiil</label>
                    <div class="input-group">
                        <select class="js-example-basic-multiple-no-tags col-sm-12 @error('skill') is-invalid @enderror" name="skill" multiple="multiple">
                            @foreach($skills as $skill)
                            <option value="{{ $skill->label }}">{{ $skill->label }}</option>
                            @endforeach
                        </select>
                        @error('job_description')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>

            <button class="btn btn-danger" data-repeater-delete type="button">Delete</button>
        </div>
    </div>
    <br />
    <button class="btn btn-success" data-repeater-create type="button">+ Add</button>
</div>
<br /><br />