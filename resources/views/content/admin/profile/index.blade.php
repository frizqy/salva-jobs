@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="edit-profile">
        <div class="row">
            <div class="col-xl-4">
                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">My Profile</h4>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row mb-2">
                                <div class="profile-title">
                                    <div class="media">
                                        <img class="img-70 rounded-circle" style="height:70px !important" alt="" src="{{ $user->photo_profile }}">
                                        <div class="media-body">
                                            <h3 class="mb-1 f-20 txt-primary">{{ $user->first_name }} {{ $user->last_name }}</h3>
                                            @if($user->job_title != null)<p class="f-12">{{ $user->job_title->label }}</p>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">First Name</label>
                                <input disabled class="form-control" value="{{ $user->first_name }}">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Last Name</label>
                                <input disabled class="form-control" value="{{ $user->last_name }}">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Email-Address</label>
                                <input disabled class="form-control" value="{{ $user->email }}">
                            </div>

                            <!-- <div class="form-footer">
                                <button class="btn btn-primary btn-block">Save</button>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-8">
                <form class="card" method="POST" action="{{ route('update-profile') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">Edit Profile</h4>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Company</label>
                                    <input class="form-control" type="text" disabled value="@if($user->company != null) {{ $user->company->name }} @endif">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Email address</label>
                                    <input class="form-control" type="email" disabled value="{{ $user->email }}">
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">First Name *</label>
                                    <input class="form-control" type="text" value="{{ $user->first_name }}" name="first_name">
                                    @error('first_name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Last Name *</label>
                                    <input class="form-control" type="text" value="{{ $user->last_name }}" name="last_name">
                                    @error('last_name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Job Title</label>
                                    <select class="js-example-basic-single col-sm-12 @error('job_titles_id') is-invalid @enderror" name="job_title_id">
                                        @foreach($job_titles as $job_title)
                                        <option @if(old('job_title_id', ($user->job_title_id != null) ? $user->job_title_id : '') == $job_title->id) selected @endif value="{{ $job_title->id }}">{{ $job_title->label }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Profile Pic</label>
                                    <input class="form-control @error('photo_profile') is-invalid @enderror" type="file" name="photo_profile">
                                    @error('photo_profile')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-end">
                        <button class="btn btn-primary" type="submit">Update Profile</button>
                    </div>
                </form>

                <form class="card" method="POST" action="{{ route('change-password') }}">
                    @csrf
                    <div class="card-header pb-0">
                        <h4 class="card-title mb-0">Change Password</h4>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-bs-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Current Password *</label>
                                    <input class="form-control" type="password" placeholder="" name="current_password">
                                    @error('current_password')
                                    <div style="color:red"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                </div>
                            </div>


                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">New Password *</label>
                                    <input class="form-control" type="password" placeholder="" name="new_password">
                                    @error('new_password')
                                    <div style="color:red"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">Confirm New Password *</label>
                                    <input class="form-control" type="password" placeholder="" name="new_password_confirmation">
                                    @error('new_password_confirm')
                                    <div style="color:red"><small>{{ $message }}</small></div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card-footer text-end">
                        <button class="btn btn-primary" type="submit">Change Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->

@endsection