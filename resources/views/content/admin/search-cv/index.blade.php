@extends('layouts/base')

@section('content')
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row learning-block">
        <div class="col-sm-9">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content" id="top-tabContent">
                        <div class="tab-pane fade show active" id="top-home" role="tabpanel" aria-labelledby="top-home-tab">
                            <div class="row">
                                <div class="col-xxl-6 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Doing</span>
                                        <h6>Endless admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Themeforest, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-primary">12 </div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-primary">5</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-primary">7</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+10 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>70% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-primary progress-bar-striped" role="progressbar" style="width: 70%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Universal admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">24</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">24</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">40</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+3 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Poco admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">40</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">40</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">20</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+2 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Universal admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">24</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">24</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">40</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+3 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Doing</span>
                                        <h6>Endless admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Themeforest, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-primary">12 </div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-primary">5</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-primary">7</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+10 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>70% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-primary progress-bar-striped" role="progressbar" style="width: 70%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-6 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Poco admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">40</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">40</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">20</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+2 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="top-profile" role="tabpanel" aria-labelledby="profile-top-tab">
                            <div class="row">
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Doing</span>
                                        <h6>Endless admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Themeforest, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-primary">12 </div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-primary">5</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-primary">7</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+10 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>70% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-primary progress-bar-striped" role="progressbar" style="width: 70%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Doing</span>
                                        <h6>Universal admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-primary">24</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-primary">24</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-primary">40</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+3 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-primary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Doing</span>
                                        <h6>Poco admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-primary">40</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-primary">40</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-primary">20</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+2 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-primary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Doing</span>
                                        <h6>Universal admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-primary">24</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-primary">24</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-primary">40</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+3 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-primary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Doing</span>
                                        <h6>Endless admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Themeforest, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-primary">12 </div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-primary">5</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-primary">7</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+10 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>70% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-primary progress-bar-striped" role="progressbar" style="width: 70%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Doing</span>
                                        <h6>Poco admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-primary">40</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-primary">40</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-primary">20</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+2 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-primary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="top-contact" role="tabpanel" aria-labelledby="contact-top-tab">
                            <div class="row">
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Endless admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Themeforest, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">12 </div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">5</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">7</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+10 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>70% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary progress-bar-striped" role="progressbar" style="width: 70%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Universal admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">24</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">24</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">40</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+3 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Poco admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">40</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">40</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">20</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+2 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Universal admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">24</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">24</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">40</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+3 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-primary">Done</span>
                                        <h6>Endless admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Themeforest, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">12 </div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">5</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">7</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+10 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>70% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary progress-bar-striped" role="progressbar" style="width: 70%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-lg-6">
                                    <div class="project-box"><span class="badge badge-secondary">Done</span>
                                        <h6>Poco admin Design</h6>
                                        <div class="media"><img class="img-20 me-2 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title="">
                                            <div class="media-body">
                                                <p>Envato, australia</p>
                                            </div>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        <div class="row details">
                                            <div class="col-6"><span>Issues </span></div>
                                            <div class="col-6 font-secondary">40</div>
                                            <div class="col-6"> <span>Resolved</span></div>
                                            <div class="col-6 font-secondary">40</div>
                                            <div class="col-6"> <span>Comment</span></div>
                                            <div class="col-6 font-secondary">20</div>
                                        </div>
                                        <div class="customers">
                                            <ul>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/3.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/5.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block"><img class="img-30 rounded-circle" src="../assets/images/user/1.jpg" alt="" data-original-title="" title=""></li>
                                                <li class="d-inline-block ms-2">
                                                    <p class="f-12">+2 More</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="project-status mt-4">
                                            <div class="media mb-0">
                                                <p>100% </p>
                                                <div class="media-body text-end"><span>Done</span></div>
                                            </div>
                                            <div class="progress" style="height: 5px">
                                                <div class="progress-bar-animated bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 xl-40">
            <div class="job-sidebar"><a class="btn btn-primary job-toggle" href="javascript:void(0)">learning filter</a>
                <div class="job-left-aside custom-scrollbar">
                    <div class="default-according style-1 faq-accordion job-accordion" id="accordionoc">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-0 p-0">
                                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseicon" aria-expanded="true" aria-controls="collapseicon">Find Course</button>
                                        </h5>
                                    </div>
                                    <div class="collapse show" id="collapseicon" aria-labelledby="collapseicon" data-bs-parent="#accordion">
                                        <div class="card-body filter-cards-view animate-chk">
                                            <div class="job-filter">
                                                <div class="faq-form">
                                                    <input class="form-control" type="text" placeholder="Search.."><i class="search-icon" data-feather="search"></i>
                                                </div>
                                            </div>
                                            <div class="checkbox-animated">
                                                <div class="learning-header"><span class="f-w-600">Categories</span></div>
                                                <label class="d-block" for="chk-ani">
                                                    <input class="checkbox_animated" id="chk-ani" type="checkbox"> Accounting
                                                </label>
                                                <label class="d-block" for="chk-ani0">
                                                    <input class="checkbox_animated" id="chk-ani0" type="checkbox"> Design
                                                </label>
                                                <label class="d-block" for="chk-ani1">
                                                    <input class="checkbox_animated" id="chk-ani1" type="checkbox"> Development
                                                </label>
                                                <label class="d-block" for="chk-ani2">
                                                    <input class="checkbox_animated" id="chk-ani2" type="checkbox"> Management
                                                </label>
                                            </div>
                                            <div class="checkbox-animated mt-0">
                                                <div class="learning-header"><span class="f-w-600">Duration</span></div>
                                                <label class="d-block" for="chk-ani6">
                                                    <input class="checkbox_animated" id="chk-ani6" type="checkbox"> 0-50 hours
                                                </label>
                                                <label class="d-block" for="chk-ani7">
                                                    <input class="checkbox_animated" id="chk-ani7" type="checkbox"> 50-100 hours
                                                </label>
                                                <label class="d-block" for="chk-ani8">
                                                    <input class="checkbox_animated" id="chk-ani8" type="checkbox"> 100+ hours
                                                </label>
                                            </div>
                                            <div class="checkbox-animated mt-0">
                                                <div class="learning-header"><span class="f-w-600">Price</span></div>
                                                <label class="d-block" for="edo-ani">
                                                    <input class="radio_animated" id="edo-ani" type="radio" name="rdo-ani" checked=""> All Courses
                                                </label>
                                                <label class="d-block" for="edo-ani1">
                                                    <input class="radio_animated" id="edo-ani1" type="radio" name="rdo-ani" checked=""> Paid Courses
                                                </label>
                                                <label class="d-block" for="edo-ani2">
                                                    <input class="radio_animated" id="edo-ani2" type="radio" name="rdo-ani" checked=""> Free Courses
                                                </label>
                                            </div>
                                            <div class="checkbox-animated mt-0">
                                                <div class="learning-header"><span class="f-w-600">Work Preferences</span></div>
                                                <label class="d-block" for="chk-ani3">
                                                    <input class="checkbox_animated" id="chk-ani3" type="checkbox"> Registration
                                                </label>
                                                <label class="d-block" for="chk-ani4">
                                                    <input class="checkbox_animated" id="chk-ani4" type="checkbox"> Progress
                                                </label>
                                                <label class="d-block" for="chk-ani5">
                                                    <input class="checkbox_animated" id="chk-ani5" type="checkbox"> Completed
                                                </label>
                                            </div>
                                            <button class="btn btn-primary text-center" type="button">Filter</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection

@section('content-js')
<script src="{{ asset('assets/js/page/candidate/index.js') }}"></script>
@endsection