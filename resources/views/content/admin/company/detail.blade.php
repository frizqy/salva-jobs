@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0">
                    <h5>Company Detail</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Company Logo</label>
                                <br />
                                @if($company->image != '')
                                <img class="img-thumbnail" src="/{{ $company->image }}" itemprop="thumbnail" alt="{{ $company->name }}" title="{{ $company->name }}">
                                @else
                                -
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Company Name</label>
                                <input class="form-control" type="text" value="{{ $company->name }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Address</label>
                                <textarea class="form-control" rows="2" disabled>{{ $company->address }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Company Size</label>
                                <textarea class="form-control" rows="2" disabled>@if($company->employee_size != null) {{ $company->employee_size->label }} @endif</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Industry</label>
                                <textarea class="form-control" rows="2" disabled>@if($company->industry != null) {{ $company->industry->label }} @endif</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Perk & Benefit *</label>
                                <textarea class="form-control" rows="5" disabled>{{ $company->benefit }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="mb-3">
                            <div class="col-xl-12 xl-100 xl-mt-job">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="form-label">Website</label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="inputGroupPrepend"><i data-feather="globe"></i></span>
                                            <input class="form-control" type="text" disabled value="{{ $company->website }}" placeholder="Company Website">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="form-label">Phone Number</label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="inputGroupPrepend"><i data-feather="phone"></i></span>
                                            <input class="form-control" type="text" disabled value="{{ $company->phone_number }}" placeholder="Company Phone Number">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="mb-3">
                            <div class="col-xl-12 xl-100 xl-mt-job">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="form-label">Social Media</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="form-label">Instagram</label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="inputGroupPrepend"><i data-feather="instagram"></i></span>
                                            <input class="form-control" type="text" disabled value="{{ $company->instagram }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-label">Facebook</label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="inputGroupPrepend"><i data-feather="facebook"></i></span>
                                            <input class="form-control" type="text" disabled value="{{ $company->facebook }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-label">LinkedIn</label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="inputGroupPrepend"><i data-feather="linkedin"></i></span>
                                            <input class="form-control" type="text" disabled value="{{ $company->linkedin }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label">Description</label>
                                <textarea class="form-control" rows="5" disabled>{{ $company->description }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-end">
                    <a href="{{ route('admin-company-edit', ['id' => $company->id]) }}" class="btn btn-primary">Edit</a>
                    <a href="{{ route('admin-company-index') }}" class="btn btn-light">Back</a>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- Container-fluid Ends-->

@endsection