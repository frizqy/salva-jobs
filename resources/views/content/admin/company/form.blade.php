@extends('layouts/base')

@section('content')

<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header pb-0">
                    <h5>Company</h5>
                </div>
                @if($action == 'edit')
                <form class="form theme-form" method="POST" action="{{ route('admin-company-update', ['id' => $company->id]) }}" enctype="multipart/form-data">
                    @else
                    <form class="form theme-form" method="POST" action="{{ route('admin-company-store') }}" enctype="multipart/form-data">
                        @endif
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        @if(isset($company) && $company->image != '')
                                        <img class="img-thumbnail" src="/{{ $company->image }}" itemprop="thumbnail" alt="{{ $company->name }}" title="{{ $company->name }}">
                                        @endisset
                                        <br/>
                                        <br/>
                                        <label class="form-label">Company Logo</label>
                                        <input class="form-control @error('image') is-invalid @enderror" type="file" name="image">
                                        @error('image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Company Name *</label>
                                        <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name', (isset($company)) ? $company->name : '') }}">
                                        @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Address *</label>
                                        <textarea class="form-control @error('address') is-invalid @enderror" rows="2" name="address">{{ old('address', (isset($company)) ? $company->address : '') }}</textarea>
                                        @error('address')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Company Size *</label>
                                        <select class="js-example-basic-single col-sm-12 @error('employee_size_id') is-invalid @enderror" name="employee_size_id">
                                            @foreach($attributes as $attribute)
                                            @if($attribute->group == 'company_size')
                                            <option @if(old('employee_size_id', (isset($company)) ? $company->employee_size_id : '') == $attribute->id) selected @endif value="{{ $attribute->id }}">{{ $attribute->label }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @error('employee_size_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Industry *</label>
                                        <select class="js-example-basic-single col-sm-12 @error('employee_size_id') is-invalid @enderror" name="industry_id">
                                            @foreach($attributes as $attribute)
                                            @if($attribute->group == 'company_industries')
                                            <option @if(old('industry_id', (isset($company)) ? $company->industry_id : '') == $attribute->id) selected @endif value="{{ $attribute->id }}">{{ $attribute->label }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @error('industry_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Perk & Benefit *</label>
                                        <textarea class="form-control @error('benefit') is-invalid @enderror" rows="5" name="benefit">{{ old('benefit', (isset($company)) ? $company->benefit : '') }}</textarea>
                                        @error('benefit')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3">
                                    <div class="col-xl-12 xl-100 xl-mt-job">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="form-label">Website</label>
                                                <div class="input-group">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i data-feather="globe"></i></span>
                                                    <input class="form-control @error('website') is-invalid @enderror" type="text" name="website" value="{{ old('website', (isset($company)) ? $company->website : '') }}" placeholder="Company Website">
                                                    @error('website')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="form-label">Phone Number</label>
                                                <div class="input-group">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i data-feather="phone"></i></span>
                                                    <input class="form-control @error('phone_number') is-invalid @enderror" type="text" name="phone_number" value="{{ old('phone_number', (isset($company)) ? $company->phone_number : '') }}" placeholder="Company Phone Number">
                                                    @error('phone_number')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="mb-3">
                                    <div class="col-xl-12 xl-100 xl-mt-job">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="form-label">Social Media</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="form-label">Instagram</label>
                                                <div class="input-group">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i data-feather="instagram"></i></span>
                                                    <input class="form-control @error('instagram') is-invalid @enderror" type="text" name="instagram" value="{{ old('instagram', (isset($company)) ? $company->instagram : '') }}" placeholder="Company Instagram">
                                                    @error('instagram')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-label">Facebook</label>
                                                <div class="input-group">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i data-feather="facebook"></i></span>
                                                    <input class="form-control @error('facebook') is-invalid @enderror" type="text" name="facebook" value="{{ old('facebook', (isset($company)) ? $company->facebook : '') }}" placeholder="Company Facebook">
                                                    @error('facebook')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-label">LinkedIn</label>
                                                <div class="input-group">
                                                    <span class="input-group-text" id="inputGroupPrepend"><i data-feather="linkedin"></i></span>
                                                    <input class="form-control @error('linkedin') is-invalid @enderror" type="text" name="linkedin" value="{{ old('linkedin', (isset($company)) ? $company->linkedin : '') }}" placeholder="Company Linkedin">
                                                    @error('linkedin')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label">Description *</label>
                                        <textarea class="form-control @error('description') is-invalid @enderror" rows="5" name="description">{{ old('description', (isset($company)) ? $company->description : '') }}</textarea>
                                        @error('description')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer text-end">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <a href="{{ route('admin-company-index') }}" class="btn btn-light">Cancel</a>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
<!-- Container-fluid Ends-->

@endsection