@extends('layouts/base')

@section('content')
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        @include('content.job.filter')
        @php
        $now = Carbon\Carbon::now();
        @endphp
        <div class="col-xl-9 xl-60 box-col-8">
            <div class="card">
                <div class="job-search">
                    <div class="card-body">
                        <div class="media">
                            <img style="object-fit: cover; width:90px !important; height:90px !important;" class="img-fluid m-r-20" src="/{{ $job->company->image }}" alt="{{ $job->company->name }}" title="{{ $job->company->name }}">
                            <div class="media-body">
                                @php
                                $days_count = 'Posted on: ' . Carbon\Carbon::parse($job->created_at)->format('d M Y');

                                $sallary = 'Confidential';
                                if($job->min_sallary != 0 && $job->max_sallary != 0){
                                $sallary = 'IDR ' . number_format($job->min_sallary, 0, ",", ".") . " - " . number_format($job->max_sallary, 0, ",", ".");
                                }
                                @endphp
                                <h6 class="f-w-600">
                                    <a href="{{ route('job-detail', ['slug' => $job->slug]) }}">{{ $job->title }}</a>
                                    <span class="pull-right">{{ $days_count }}</span>
                                </h6>
                                <p>
                                    {{ $job->company->name }}
                                    <br />
                                    <span><i style="width:14px !important; height:14px !important;" class="mr-5" data-feather="map-pin"></i>&nbsp;{{ $job->work_location }}</span>
                                    <br />
                                    <span><i style="width:14px !important; height:14px !important;" class="mr-5" data-feather="credit-card"></i>&nbsp;{{ $sallary }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                &nbsp;
                            </div>
                            <a class="btn btn-primary btn-sm job-apply-btn" href="{{ route('job-apply', ['id' => 249182]) }}">Apply for this job</a>
                        </div>
                        <div class="job-description">
                            <p class="text-start">{!! $job->description !!}</p>
                        </div>
                        <div class="job-description">
                            <h6>Job Description</h6>
                            <p class="text-start">{!! $job->job_description !!}</p>
                        </div>
                        <div class="job-description">
                            <h6>Qualifications</h6>
                            <p class="text-start">{!! $job->qualification !!}</p>
                        </div>
                        <div class="job-description">
                            <h6>Requirement</h6>
                            <p class="text-start">{!! $job->requirement !!}</p>
                        </div>
                        <div class="job-description">
                            <h6>Benefit</h6>
                            <p class="text-start">{!! $job->benefit !!}</p>
                        </div>

                        <div class="job-description">
                            <div class="row">
                                <div class="col-sm-12 col-xl-6">
                                    <h6><small>Career Level</small></h6>
                                    {{ $job->career_level->label }}
                                    <br />
                                    <br />
                                    <h6><small>Year of Experience</small></h6>
                                    {{ $job->year_experience }}
                                </div>
                                <div class="col-sm-12 col-xl-6">
                                    <h6><small>Job Type</small></h6>
                                    {{ $job->job_type->label }}
                                    <br />
                                    <br />
                                    <h6><small>Job Specialization</small></h6>
                                    {{ $job->job_specialization }}
                                </div>
                            </div>
                        </div>

                        <div class="job-description">
                            <h6>Company Overview</h6>
                            <p class="text-start">{!! $job->company->address !!}</p>
                            <p class="text-start">{!! $job->company->description !!}</p>

                            <div class="row">
                                <div class="col-sm-12 col-xl-6">
                                    <h6><small>Number of employee</small></h6>
                                    {{ $job->company->employee_size->label }}
                                </div>
                                <div class="col-sm-12 col-xl-6">
                                    <h6><small>Industry</small></h6>
                                    {{ $job->company->industry->label }}
                                </div>
                            </div>
                        </div>

                        <div class="job-description">
                            <button class="btn btn-primary" type="button"><span><i class="fa fa-check"></i></span> Save this job</button>
                            {{-- <button class="btn btn-primary" type="button"><span><i class="fa fa-share-alt"></i></span> Share</button> --}}
                        </div>
                    </div>
                </div>
            </div>

            {{--
            @include('content.job.similar')
            --}}
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection