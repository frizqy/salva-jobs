@extends('layouts/base')

@section('content')
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        @include('content.job.filter')
        <div class="col-xl-9 xl-60 box-col-8">

            @php
            $now = Carbon\Carbon::now();
            @endphp

            @foreach ($jobs as $job)
            <div class="card">
                <div class="job-search">
                    <div class="card-body">
                        <div class="media">
                            <img style="object-fit: cover; width:75px !important; height:75px !important;" class="img-fluid m-r-20" src="/{{ $job->company->image }}" alt="{{ $job->company->name }}" title="{{ $job->company->name }}">
                            <div class="media-body">
                                @php
                                $days_count = Carbon\Carbon::parse($job->created_at)->diffInDays($now);
                                if($days_count > 3){
                                    $days_count = 'Posted on: ' . Carbon\Carbon::parse($job->created_at)->format('d M Y');
                                } else {
                                    $days_count = $days_count . ' days ago';
                                }
                                @endphp
                                <h6 class="f-w-600">
                                    <a href="{{ route('job-detail', ['slug' => $job->slug]) }}">{{ $job->title }}</a>&nbsp;&nbsp;<span class="badge rounded-pill badge-secondary"><small>{{ $job->job_type->label }}</small></span>
                                    <span class="pull-right">{{ $days_count }}</span>
                                </h6>
                                <p>
                                    {{ $job->company->name }}
                                    <br />
                                    <span><i style="width:14px !important; height:14px !important;" class="mr-5" data-feather="map-pin"></i>&nbsp;{{ $job->work_location }}</span>
                                </p>
                            </div>
                        </div>
                        <p>{!! $job->description !!}</p>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="job-pagination">
                {!! $jobs->appends(Request::all())->links('layouts.pagination') !!}
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection