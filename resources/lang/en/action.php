<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'create_success' => 'Data has been created.',
    'create_failed' => 'Create data failed.',
    'update_success' => 'Data has been updated.',
    'update_failed' => 'Update data failed.',
    'delete_success' => 'Data has been deleted.',
    'delete_failed' => 'Delete data failed.',

];
