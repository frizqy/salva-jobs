<?php

namespace App\Services;

use App\Traits\ApiResponseTrait;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BaseService implements ServiceInterface
{
    use ApiResponseTrait;

    protected $repo;
    protected $object;
    protected $filterClass;
    protected $indexWith = [];
    protected $detailWith = [];

    protected $connection = 'pgsql_client';

    public function __construct()
    {
        if (App::environment('testing')) {
            $this->connection = 'sqlite';
        }
    }

    public function all(array $request = null)
    {
        $datas = $this->repo->with($this->indexWith)->all($request, $this->filterClass);
        $success = $datas;
        return $this->successResponse($success, __('content.message.default.success'));
    }

    public function detail($id)
    {
        $data = $this->repo->with($this->detailWith)->find($id);
        $success['data'] = $data;

        return $this->successResponse($success, __('content.message.default.success'));
    }

    public function create(array $data)
    {
        try {
            $execute = DB::transaction(function () use ($data) {
                $created = $this->repo->create($data);
                return $created->refresh();
            });

            $success['data'] = $execute;
            return $this->successResponse($success, __('content.message.create.success'), 201);
        } catch (Exception $exc) {
            Log::error($exc);
            $failed['exception'] = $exc->getMessage();
            return $this->failedResponse(null, __('content.message.create.failed'), 400, $failed);
        }
    }

    public function update(array $data, $id)
    {
        try {
            $execute = DB::transaction(function () use ($data, $id) {
                $updated = $this->repo->update($data, $id);
                return $updated;
            });

            $success['data'] = $execute;
            return $this->successResponse($success, __('content.message.update.success'));
        } catch (Exception $exc) {
            Log::error($exc);
            $failed['exception'] = $exc->getMessage();
            return $this->failedResponse(null, __('content.message.create.failed'), 400, $failed);
        }
    }

    public function destroy($id)
    {
        try {
            $execute = DB::transaction(function () use ($id) {
                return $this->repo->destroy($id);
            });

            $success['data'] = $execute;
            return $this->successResponse($success, __('content.message.delete.success'));
        } catch (Exception $exc) {
            Log::error($exc);
            $failed['exception'] = $exc->getMessage();
            return $this->failedResponse(null, __('content.message.delete.failed'), 400, $failed);
        }
    }

    public function uploadFile(array $data, $key, $rename = false, $filename = "")
    {
        if (isset($data[$key])) {
            $file = $data[$key];
            $filepath = 'public/' . $this->uploadFolder;
            $extension = $file->extension();

            $uploadedFile = Storage::put($filepath, $file);
            // $filename = basename($uploadedFile);
            if ($rename && $filename !== "") {
                $newFilename = $filepath . '/' . $filename . '.' . $extension;
                if (Storage::exists($newFilename)) {
                    Storage::delete($newFilename);
                }
                Storage::move($uploadedFile, $newFilename);
            } else {
                $newFilename = $uploadedFile;
            }


            $data[$key] = $newFilename;
        }

        return $data;
    }

    public function masterUpdateOrCreate($datas)
    {
        $labels = [];
        foreach ($datas as $data) {
            $code = strtolower(str_replace(" ", "_", $data));
            $attrs = [];
            $attrs['code'] = $code;
            $attrs['label'] = $data;
            $this->repo->updateOrCreate($attrs, $attrs);

            array_push($labels, $data);
        }

        return $labels;
    }
}
