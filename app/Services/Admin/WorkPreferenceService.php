<?php

namespace App\Services\Admin;

use App\Filters\Admin\WorkPreferenceFilter;
use App\Repositories\Admin\WorkPreferenceRepository;
use App\Services\BaseService;

class WorkPreferenceService extends BaseService
{
    public function __construct(WorkPreferenceRepository $repo, WorkPreferenceFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Job";
        $this->filterClass = $filter;
    }
}