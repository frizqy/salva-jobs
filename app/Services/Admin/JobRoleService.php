<?php

namespace App\Services\Admin;

use App\Filters\Admin\JobRoleFilter;
use App\Repositories\Admin\JobRoleRepository;
use App\Services\BaseService;

class JobRoleService extends BaseService
{
    public function __construct(JobRoleRepository $repo, JobRoleFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Job Role";
        $this->filterClass = $filter;

        $this->indexWith = ['job_specialization'];
    }
}
