<?php

namespace App\Services\Admin;

use App\Filters\Admin\JobTypeFilter;
use App\Repositories\Admin\JobTypeRepository;
use App\Services\BaseService;

class JobTypeService extends BaseService
{
    public function __construct(JobTypeRepository $repo, JobTypeFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Job";
        $this->filterClass = $filter;
    }
}