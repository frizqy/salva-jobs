<?php

namespace App\Services\Admin;

use App\Filters\Admin\CandidateFilter;
use App\Jobs\CandidateInvitationJob;
use App\Mail\CandidateInvitationEmail;
use App\Models\Admin\CandidateCertification;
use App\Models\Admin\CandidateEducation;
use App\Models\Admin\CandidateLanguage;
use App\Models\Admin\CandidateSkill;
use App\Models\Admin\CandidateWorkExperience;
use App\Repositories\Admin\CandidateCertificationRepository;
use App\Repositories\Admin\CandidateEducationRepository;
use App\Repositories\Admin\CandidateLanguageRepository;
use App\Repositories\Admin\CandidateRepository;
use App\Repositories\Admin\CandidateSkillRepository;
use App\Repositories\Admin\CandidateWorkExperienceRepository;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CandidateService extends BaseService
{
    protected $candidateCertificationRepo, $candidateEducationRepo, $candidateLanguageRepo, $candidateSkillRepo, $candidateWorkExperienceRepo;

    public function __construct(
        CandidateRepository $repo,
        CandidateFilter $filter,
        CandidateCertificationRepository $candidateCertificationRepo,
        CandidateEducationRepository $candidateEducationRepo,
        CandidateLanguageRepository $candidateLanguageRepo,
        CandidateSkillRepository $candidateSkillRepo,
        CandidateWorkExperienceRepository $candidateWorkExperienceRepo
    ) {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Candidate";
        $this->filterClass = $filter;

        $this->candidateCertificationRepo = $candidateCertificationRepo;
        $this->candidateEducationRepo = $candidateEducationRepo;
        $this->candidateLanguageRepo = $candidateLanguageRepo;
        $this->candidateSkillRepo = $candidateSkillRepo;
        $this->candidateWorkExperienceRepo = $candidateWorkExperienceRepo;

        $this->detailWith = ['educations', 'work_experiences', 'skills.skill', 'certifications', 'languages', 'job_specialization', 'job_role'];
    }

    public function create(array $data)
    {
        try {
            $execute = DB::transaction(function () use ($data) {

                $dataEducations = isset($data['educations']) ? $data['educations'] : [];
                $dataWorkExperiences = isset($data['work_experiences']) ? $data['work_experiences'] : [];
                $dataSkills = isset($data['skill']) ? $data['skill'] : [];
                $dataCertifications = isset($data['certifications']) ? $data['certifications'] : [];
                $dataLanguages = isset($data['languages']) ? $data['languages'] : [];

                $data = Arr::except($data, 'educations');
                $data = Arr::except($data, 'work_experiences');
                $data = Arr::except($data, 'skill');
                $data = Arr::except($data, 'certifications');
                $data = Arr::except($data, 'languages');

                $data['current_sallary'] = isset($data['current_sallary']) ? str_replace('.', '', $data['current_sallary']) : 0;
                $data['expected_sallary'] = isset($data['expected_sallary']) ? str_replace('.', '', $data['expected_sallary']) : 0;
                $created = $this->repo->create($data);

                $educations = [];
                foreach ($dataEducations as $item) {
                    $item['name'] = $item['education_name'];
                    $item['degree'] = $item['education_degree'];
                    $item = Arr::except($item, 'education_name');
                    $item = Arr::except($item, 'education_degreee');
                    $educations[] = new CandidateEducation($item);
                }


                $workExperiences = [];
                foreach ($dataWorkExperiences as $item) {
                    $item['used_technology'] = implode(', ', $item['skill']);
                    $workExperiences[] = new CandidateWorkExperience($item);
                }

                $skills = [];
                foreach ($dataSkills as $item) {
                    $arrItem = [];
                    $arrItem['skill_id'] = $item;
                    $skills[] = new CandidateSkill($arrItem);
                }

                $certifications = [];
                foreach ($dataCertifications as $item) {
                    $item['name'] = $item['event_name'];
                    $item['year'] = $item['event_year'];
                    $certifications[] = new CandidateCertification($item);
                }

                $languages = [];
                foreach ($dataLanguages as $item) {
                    $languages[] = new CandidateLanguage($item);
                }

                $createdEdutcations = $created->educations()->saveMany($educations);
                $createdWorkExperiences = $created->work_experiences()->saveMany($workExperiences);
                $createSkills = $created->skills()->saveMany($skills);
                $createCertifications = $created->certifications()->saveMany($certifications);
                $createLanguages = $created->languages()->saveMany($languages);

                $candidate = $created->refresh();
                // $this->sendEmail($candidate->id);
                return $candidate;
            });

            $success['data'] = $execute;
            return $this->successResponse($success, __('content.message.create.success'), 201);
        } catch (Exception $exc) {
            Log::error($exc);
            $failed['exception'] = $exc->getMessage();
            return $this->failedResponse(null, __('content.message.create.failed'), 400, $failed);
        }
    }

    public function update(array $data, $id)
    {
        try {
            $execute = DB::transaction(function () use ($data, $id) {

                $dataEducations = isset($data['educations']) ? $data['educations'] : [];
                $dataWorkExperiences = isset($data['work_experiences']) ? $data['work_experiences'] : [];
                $dataSkills = isset($data['skill']) ? $data['skill'] : [];
                $dataCertifications = isset($data['certifications']) ? $data['certifications'] : [];
                $dataLanguages = isset($data['languages']) ? $data['languages'] : [];

                $data = Arr::except($data, 'educations');
                $data = Arr::except($data, 'work_experiences');
                $data = Arr::except($data, 'skill');
                $data = Arr::except($data, 'certifications');
                $data = Arr::except($data, 'languages');

                $data['current_sallary'] = isset($data['current_sallary']) ? str_replace('.', '', $data['current_sallary']) : 0;
                $data['expected_sallary'] = isset($data['expected_sallary']) ? str_replace('.', '', $data['expected_sallary']) : 0;

                $updated = $this->repo->update($data, $id);

                $educations = [];
                $existsEducationIds = [];
                foreach ($dataEducations as $item) {
                    $item['name'] = $item['education_name'];
                    $item['degree'] = $item['education_degree'];
                    $item = Arr::except($item, 'education_name');
                    $item = Arr::except($item, 'education_degreee');

                    if ($item['id'] == null) {
                        $educations[] = new CandidateEducation($item);
                    } else {
                        $this->candidateEducationRepo->update($item, $item['id']);
                        array_push($existsEducationIds, $item['id']);
                    }
                }


                $workExperiences = [];
                $existsWorkExperinceIds = [];
                foreach ($dataWorkExperiences as $item) {
                    $item['used_technology'] = implode(', ', $item['skill']);

                    if ($item['id'] == null) {
                        $workExperiences[] = new CandidateWorkExperience($item);
                    } else {
                        $this->candidateWorkExperienceRepo->update($item, $item['id']);
                        array_push($existsWorkExperinceIds, $item['id']);
                    }
                }

                $skills = [];
                foreach ($dataSkills as $item) {
                    $arrItem = [];
                    $arrItem['skill_id'] = $item;
                    $skills[] = new CandidateSkill($arrItem);
                }

                $certifications = [];
                $existsCertificationIds = [];
                foreach ($dataCertifications as $item) {
                    $item['name'] = $item['event_name'];
                    $item['year'] = $item['event_year'];

                    if ($item['id'] == null) {
                        $certifications[] = new CandidateCertification($item);
                    } else {
                        $this->candidateCertificationRepo->update($item, $item['id']);
                        array_push($existsCertificationIds, $item['id']);
                    }
                }

                $languages = [];
                $existsLanguageIds = [];
                foreach ($dataLanguages as $item) {
                    if ($item['id'] == null) {
                        $languages[] = new CandidateLanguage($item);
                    } else {
                        $this->candidateLanguageRepo->update($item, $item['id']);
                        array_push($existsLanguageIds, $item['id']);
                    }
                }


                $this->candidateSkillRepo->destroyByCandidate($id);

                $this->candidateEducationRepo->deleteWhereNotIn($existsEducationIds, $id);
                $this->candidateWorkExperienceRepo->deleteWhereNotIn($existsWorkExperinceIds, $id);
                $this->candidateCertificationRepo->deleteWhereNotIn($existsCertificationIds, $id);
                $this->candidateLanguageRepo->deleteWhereNotIn($existsLanguageIds, $id);

                $createdEdutcations = $updated->educations()->saveMany($educations);
                $createdWorkExperiences = $updated->work_experiences()->saveMany($workExperiences);
                $createSkills = $updated->skills()->saveMany($skills);
                $createCertifications = $updated->certifications()->saveMany($certifications);
                $createLanguages = $updated->languages()->saveMany($languages);

                return $updated->refresh();
                // return $updated;
            });

            $success['data'] = $execute;
            return $this->successResponse($success, __('content.message.update.success'));
        } catch (Exception $exc) {
            Log::error($exc);
            $failed['exception'] = $exc->getMessage();
            return $this->failedResponse(null, __('content.message.create.failed'), 400, $failed);
        }
    }

    public function sendEmail($id)
    {
        $data = $this->repo->find($id);
        dispatch(new CandidateInvitationJob($data));
    }
}
