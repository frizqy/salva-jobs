<?php

namespace App\Services\Admin;

use App\Filters\Admin\DegreeFilter;
use App\Repositories\Admin\DegreeRepository;
use App\Services\BaseService;

class DegreeService extends BaseService
{
    public function __construct(DegreeRepository $repo, DegreeFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Job";
        $this->filterClass = $filter;
    }
}