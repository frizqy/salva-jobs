<?php

namespace App\Services\Admin;

use App\Filters\Admin\WorkLocationFilter;
use App\Repositories\Admin\WorkLocationRepository;
use App\Services\BaseService;

class WorkLocationService extends BaseService
{
    public function __construct(WorkLocationRepository $repo, WorkLocationFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Work Location";
        $this->filterClass = $filter;
    }
}