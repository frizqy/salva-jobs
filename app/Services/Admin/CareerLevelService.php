<?php

namespace App\Services\Admin;

use App\Filters\Admin\CareerLevelFilter;
use App\Repositories\Admin\CareerLevelRepository;
use App\Services\BaseService;

class CareerLevelService extends BaseService
{
    public function __construct(CareerLevelRepository $repo, CareerLevelFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Career Level";
        $this->filterClass = $filter;
    }
}
