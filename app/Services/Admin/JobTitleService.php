<?php

namespace App\Services\Admin;

use App\Filters\Admin\JobTitleFilter;
use App\Repositories\Admin\JobTitleRepository;
use App\Services\BaseService;

class JobTitleService extends BaseService
{
    public function __construct(JobTitleRepository $repo, JobTitleFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Job Title";
        $this->filterClass = $filter;
    }
}