<?php

namespace App\Services\Admin;

use App\Filters\Admin\CompanyAttributeFilter;
use App\Repositories\Admin\CompanyAttributeRepository;
use App\Services\BaseService;

class CompanyAttributeService extends BaseService
{
    public function __construct(CompanyAttributeRepository $repo, CompanyAttributeFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Company Attribute";
        $this->filterClass = $filter;
    }
}