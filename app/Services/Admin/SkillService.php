<?php

namespace App\Services\Admin;

use App\Filters\Admin\SkillFilter;
use App\Repositories\Admin\SkillRepository;
use App\Services\BaseService;

class SkillService extends BaseService
{
    public function __construct(SkillRepository $repo, SkillFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Job";
        $this->filterClass = $filter;
    }
}