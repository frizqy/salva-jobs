<?php

namespace App\Services\Admin;

use App\Filters\Admin\JobSpecializationFilter;
use App\Repositories\Admin\JobSpecializationRepository;
use App\Services\BaseService;

class JobSpecializationService extends BaseService
{
    public function __construct(JobSpecializationRepository $repo, JobSpecializationFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Job Specialization";
        $this->filterClass = $filter;
    }
}
