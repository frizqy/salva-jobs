<?php

namespace App\Services\Admin;

use App\Filters\Admin\CompanyFilter;
use App\Repositories\Admin\CompanyRepository;
use App\Services\BaseService;

class CompanyService extends BaseService
{
    public function __construct(CompanyRepository $repo, CompanyFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Company";
        $this->filterClass = $filter;

        $this->detailWith = ['employee_size', 'industry'];
    }
}