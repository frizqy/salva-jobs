<?php

namespace App\Services\Admin;

use App\Filters\Admin\CompanyFilter;
use App\Filters\Admin\JobFilter;
use App\Repositories\Admin\JobRepository;
use App\Services\BaseService;

class JobService extends BaseService
{
    public function __construct(JobRepository $repo, JobFilter $filter)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->object = "Job";
        $this->filterClass = $filter;

        $this->indexWith = ['company', 'job_type', 'career_level'];
        $this->detailWith = ['company', 'job_type', 'career_level'];
    }
}
