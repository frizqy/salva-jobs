<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Admin\JobRole;

class JobRoleFilter extends BaseFilter
{
    public function __construct(JobRole $model)
    {
        $this->model = $model;
    }

    public function filterQ($builder, $value)
    {
        $fields = [];
        $builder = $this->qFilterFormatter($builder, $value, $fields);
        return $builder;
    }

    public function filterJobSpecializationId($builder, $search)
    {
        return $builder->where('job_specialization_id', $search);
    }
}
