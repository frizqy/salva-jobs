<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Admin\Degree;

class DegreeFilter extends BaseFilter
{
    public function __construct(Degree $model)
    {
        $this->model = $model;
    }

    public function filterQ($builder, $value)
    {
        $fields = [];
        $builder = $this->qFilterFormatter($builder, $value, $fields);
        return $builder;
    }

    //
}