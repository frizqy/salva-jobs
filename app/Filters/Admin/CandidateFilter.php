<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Admin\Candidate;

class CandidateFilter extends BaseFilter
{
    public function __construct(Candidate $model)
    {
        $this->model = $model;
    }

    public function filterQ($builder, $value)
    {
        $fields = ['name', 'email', 'instagram', 'facebook', 'linkedin', 'phone_number'];
        $builder = $this->qFilterFormatter($builder, $value, $fields);
        return $builder;
    }

    //
}