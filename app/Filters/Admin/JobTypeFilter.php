<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Admin\Job;
use App\Models\Admin\JobType;

class JobTypeFilter extends BaseFilter
{
    public function __construct(JobType $model)
    {
        $this->model = $model;
    }

    public function filterQ($builder, $value)
    {
        $fields = [];
        $builder = $this->qFilterFormatter($builder, $value, $fields);
        return $builder;
    }

    //
}