<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Admin\JobSpecialization;
use App\Models\Admin\WorkLocation;

class WorkLocationFilter extends BaseFilter
{
    public function __construct(WorkLocation $model)
    {
        $this->model = $model;
    }

    public function filterQ($builder, $value)
    {
        $fields = [];
        $builder = $this->qFilterFormatter($builder, $value, $fields);
        return $builder;
    }

    //
}