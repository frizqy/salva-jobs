<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Admin\Company;

class CompanyFilter extends BaseFilter
{
    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function filterQ($builder, $value)
    {
        $fields = ['name', 'address', 'description'];
        $builder = $this->qFilterFormatter($builder, $value, $fields);
        return $builder;
    }

    //
}