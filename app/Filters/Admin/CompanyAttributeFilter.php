<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Admin\CompanyAttribute;

class CompanyAttributeFilter extends BaseFilter
{
    public function __construct(CompanyAttribute $model)
    {
        $this->model = $model;
    }

    public function filterQ($builder, $value)
    {
        $fields = [];
        $builder = $this->qFilterFormatter($builder, $value, $fields);
        return $builder;
    }

    public function filterGroup($builder, $search)
    {
        return $builder->where('group', $search);
    }
}
