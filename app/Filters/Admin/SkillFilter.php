<?php

namespace App\Filters\Admin;

use App\Filters\BaseFilter;
use App\Models\Admin\Skill;

class SkillFilter extends BaseFilter
{
    public function __construct(Skill $model)
    {
        $this->model = $model;
    }

    public function filterQ($builder, $value)
    {
        $fields = [];
        $builder = $this->qFilterFormatter($builder, $value, $fields);
        return $builder;
    }

    //
}