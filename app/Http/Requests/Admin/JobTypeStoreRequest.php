<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class JobTypeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => ['required'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'label' => 'Name',
        ];
    }

    protected function prepareForValidation()
    {
        $label = $this->label;
        $code = str_replace([',', '-', ' ', '.'], ['_', '_', '_', '_'], strtolower($label));
        $mergeAttr['code'] = $code;

        $this->merge($mergeAttr);
    }
}
