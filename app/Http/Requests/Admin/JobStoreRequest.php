<?php

namespace App\Http\Requests\Admin;

use Facade\FlareClient\Stacktrace\File;
use Illuminate\Foundation\Http\FormRequest;

class JobStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required', 'exists:companies,id'],
            'job_type_id' => ['required', 'exists:job_types,id'],
            'year_experience' => ['required'],
            'career_level_id' => ['required', 'exists:career_levels,id'],
            'title' => ['required',],
            'job_description' => ['required',],
            'requirement' => ['required',],
            'benefit' => ['required',],
            'qualification' => ['required',],
            'open_until' => ['required', 'date_format:Y-m-d'],
            'job_specialization' => ['required'],
            'work_location' => ['required'],
            'max_sallary' => ['required', 'min:0', 'numeric', 'gte:min_sallary'],
            'min_sallary' => ['required', 'min:0', 'numeric', 'lte:max_sallary'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'company_id' => 'company',
            'job_type_id' => 'job type',
            'career_level_id' => 'career level',
        ];
    }

    protected function prepareForValidation()
    {
        $minSallary = $this->min_sallary;
        $maxSallary = $this->max_sallary;

        $mergeAttr = [];

        if ($minSallary != 0) {
            $minSallary = str_replace(".", "", $minSallary);
        }
        
        if ($maxSallary != 0) {
            $maxSallary = str_replace(".", "", $maxSallary);
        }

        $mergeAttr['min_sallary'] = (integer) $minSallary;
        $mergeAttr['max_sallary'] = (integer) $maxSallary;

        $this->merge($mergeAttr);
    }
}
