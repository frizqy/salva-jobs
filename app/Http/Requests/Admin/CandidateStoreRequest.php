<?php

namespace App\Http\Requests\Admin;

use Facade\FlareClient\Stacktrace\File;
use Illuminate\Foundation\Http\FormRequest;

class CandidateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'email' => ['required', 'email'],

            'photo_profile' => ['mimes:jpeg,png,jpg'],
            'cv' => ['mimes:pdf,doc,docx'],

            'gender' => ['in:male,female'],

            'birthday' => ['date_format:Y-m-d'],

            'job_specialization_id' => ['exists:job_specializations,id'],
            'job_role_id' => ['exists:job_roles,id'],


            'current_sallary_type' => ['in:gross,nett', 'nullable'],
            'expected_sallary_type' => ['in:gross,nett', 'nullable'],

            'skill.*' => ['exists:skills,id'],

            
        ];
    }

    public function attributes(): array
    {
        return [
            'job_specialization_id' => 'job specialization',
            'job_role_id' => 'job role',
        ];
    }
}
