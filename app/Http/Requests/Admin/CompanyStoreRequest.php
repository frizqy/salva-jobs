<?php

namespace App\Http\Requests\Admin;

use Facade\FlareClient\Stacktrace\File;
use Illuminate\Foundation\Http\FormRequest;

class CompanyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique:companies,name'],
            'address' => ['required'],
            'description' => ['required'],
            'image' => ['mimes:jpg,bmp,png,jpeg', 'max:1024'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'company name',
            'image' => 'company image',
        ];
    }
}
