<?php

namespace App\Http\Controllers;

use App\Models\Admin\Job;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->params['layout_wrapper'] = "horizontal-wrapper";
        $this->params['breadcrumbs_holder'] = false;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->params['jobs'] = Job::paginate(10);

        return view('content.home.job-list', $this->params);
    }

    public function detail(Request $request, $slug)
    {
        $job = Job::where('slug', $slug)->first();
        if ($job == null) {
            abort(404);
        }   
        $this->params['job'] = $job;
        return view('content.home.job-detail', $this->params);
    }

    public function apply(Request $request, $id)
    {
        return view('content.home.job-apply', $this->params);
    }
}
