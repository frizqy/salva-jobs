<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\SkillService;

class SkillController extends MasterGeneralController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SkillService $service)
    {
        $this->tableName = 'skills';

        parent::__construct();

        $this->service = $service;

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'Skill';

        $this->viewIndex = 'content.admin.skill.index';
        $this->viewDetail = 'content.admin.skill.form';
        $this->viewForm = 'content.admin.skill.form';

        $this->routeDetail = 'admin-skill-detail';

        $this->variableDetail = 'skill';
    }
}
