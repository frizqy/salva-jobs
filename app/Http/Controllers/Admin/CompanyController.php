<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompanyStoreRequest;
use App\Http\Requests\Admin\CompanyUpdateRequest;
use App\Models\Admin\Company;
use App\Services\Admin\CompanyAttributeService;
use App\Services\Admin\CompanyService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $companyAttributesService;

    public function __construct(CompanyService $service, CompanyAttributeService $companyAttributesService)
    {
        parent::__construct();

        $this->service = $service;

        $this->companyAttributesService = $companyAttributesService;

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;

        $this->uploadFolder = 'company';

        $this->viewIndex = 'content.admin.company.index';
        $this->viewDetail = 'content.admin.company.detail';
        $this->viewForm = 'content.admin.company.form';

        $this->routeDetail = 'admin-company-detail';
    }

    public function store(CompanyStoreRequest $request)
    {
        $attributes = [];

        $dataAttributes = $this->companyAttributesService->all($request->all());
        $companyAttributeresult = $dataAttributes->getData()->result;
        if ($companyAttributeresult != null) {
            $attributes = $companyAttributeresult->data;
        }

        $this->params['attributes'] = $attributes;

        $filename = 'company_image_' . date('YmdHi');
        $data = $this->uploadFile($request->all(), 'image', true, $filename);
        $storeData = $this->service->create($data);
        return $this->storeProcess($storeData);
    }

    public function detail(Request $request, $id)
    {
        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $this->params['company'] = $dataDetail;

        return parent::detail($request, $id);
    }

    public function edit(Request $request, $id)
    {
        
        $attributes = [];

        $dataAttributes = $this->companyAttributesService->all($request->all());
        $companyAttributeresult = $dataAttributes->getData()->result;
        if ($companyAttributeresult != null) {
            $attributes = $companyAttributeresult->data;
        }

        $this->params['attributes'] = $attributes;

        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $this->params['company'] = $dataDetail;

        return parent::edit($request, $id);
    }

    public function update(CompanyUpdateRequest $request, $id)
    {
        $filename = 'company_image_' . date('YmdHi');
        $data = $this->uploadFile($request->all(), 'image', true, $filename);
        $updatedData = $this->service->update($data, $id);
        return $this->updateProcess($updatedData);
    }
}
