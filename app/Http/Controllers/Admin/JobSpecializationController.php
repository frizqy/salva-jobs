<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\JobSpecializationService;
use Illuminate\Http\Request;

class JobSpecializationController extends MasterGeneralController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(JobSpecializationService $service)
    {
        $this->tableName = 'job_specializations';

        parent::__construct();
     
        $this->service = $service;

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'Job Specialization';

        $this->viewIndex = 'content.admin.job-specialization.index';
        $this->viewDetail = 'content.admin.job-specialization.form';
        $this->viewForm = 'content.admin.job-specialization.form';

        $this->routeDetail = 'admin-job-specialization-detail';
        
        $this->variableDetail = 'jobSpecialization';
    }
}
