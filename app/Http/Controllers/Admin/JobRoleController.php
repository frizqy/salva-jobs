<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\JobRoleService;
use App\Services\Admin\JobSpecializationService;
use Illuminate\Http\Request;

class JobRoleController extends MasterGeneralController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $jobSpecializationService;

    public function __construct(JobRoleService $service, JobSpecializationService $jobSpecializationService)
    {
        $this->tableName = 'job_roles';

        $this->service = $service;
        $this->jobSpecializationService = $jobSpecializationService;

        $jobSpecializations = [];

        $dataJobSpecializations = $this->jobSpecializationService->all([]);
        $jobSpecializationResult = $dataJobSpecializations->getData()->result;
        if ($jobSpecializationResult != null) {
            $jobSpecializations = $jobSpecializationResult->data;
        }

        $this->params['job_spzcializations'] = $jobSpecializations;

        parent::__construct();

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;

        $this->viewIndex = 'content.admin.job-role.index';
        $this->viewDetail = 'content.admin.job-role.form';
        $this->viewForm = 'content.admin.job-role.form';

        $this->routeDetail = 'admin-job-role-detail';

        $this->variableDetail = 'jobRole';
    }
}
