<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchCvController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'Search CV';

        $this->viewIndex = 'content.admin.search-cv.index';
        $this->viewDetail = 'content.admin.search-cv.detail';
    }
}
