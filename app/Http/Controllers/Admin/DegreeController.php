<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\DegreeService;
use App\Services\Admin\JobRoleService;
use Illuminate\Http\Request;

class DegreeController extends MasterGeneralController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DegreeService $service)
    {
        $this->tableName = 'degrees';

        parent::__construct();

        $this->service = $service;

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'Degree';

        $this->viewIndex = 'content.admin.degree.index';
        $this->viewDetail = 'content.admin.degree.form';
        $this->viewForm = 'content.admin.degree.form';

        $this->routeDetail = 'admin-degree-detail';

        $this->variableDetail = 'degree';
    }
}
