<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\JobStoreRequest;
use App\Http\Requests\Admin\JobUpdateRequest;
use App\Services\Admin\CareerLevelService;
use App\Services\Admin\CompanyService;
use App\Services\Admin\JobRoleService;
use App\Services\Admin\JobService;
use App\Services\Admin\JobSpecializationService;
use App\Services\Admin\JobTypeService;
use App\Services\Admin\SkillService;
use App\Services\Admin\WorkLocationService;
use App\Services\Admin\WorkPreferenceService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $jobTypeService;
    protected $companyService;
    protected $careerLevelService;
    protected $jobSpecializationService;
    protected $jobRoleService;
    protected $skillService;
    protected $workLocationService;
    protected $workPreferenceService;

    public function __construct(
        JobService $service,
        JobTypeService $jobTypeService,
        CompanyService $companyService,
        CareerLevelService $careerLevelService,
        JobSpecializationService $jobSpecializationService,
        JobRoleService $jobRoleService,
        SkillService $skillService,
        WorkLocationService $workLocationService,
        WorkPreferenceService $workPreferenceService
    ) {
        parent::__construct();

        $this->service = $service;
        $this->jobTypeService = $jobTypeService;
        $this->companyService = $companyService;
        $this->careerLevelService = $careerLevelService;
        $this->jobSpecializationService = $jobSpecializationService;
        $this->jobRoleService = $jobRoleService;
        $this->skillService = $skillService;
        $this->workLocationService = $workLocationService;
        $this->workPreferenceService = $workPreferenceService;

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;

        $this->uploadFolder = 'company';

        $this->viewIndex = 'content.admin.job.index';
        $this->viewDetail = 'content.admin.job.detail';
        $this->viewForm = 'content.admin.job.form';

        $this->routeDetail = 'admin-job-detail';
    }

    public function add(Request $request)
    {
        $jobTypes = [];
        $companies = [];
        $careerLevels = [];
        $jobSpecializations = [];
        $jobRoles = [];
        $skills = [];
        $workLocations = [];
        $workPreferences = [];

        $dataJobTypes = $this->jobTypeService->all($request->all());
        $jobTypeResult = $dataJobTypes->getData()->result;
        if ($jobTypeResult != null) {
            $jobTypes = $jobTypeResult->data;
        }

        $dataCompanies = $this->companyService->all($request->all());
        $companyResult = $dataCompanies->getData()->result;
        if ($companyResult != null) {
            $companies = $companyResult->data;
        }

        $dataCareerLevels = $this->careerLevelService->all($request->all());
        $careerLevelResult = $dataCareerLevels->getData()->result;
        if ($careerLevelResult != null) {
            $careerLevels = $careerLevelResult->data;
        }

        $dataJobSpecializations = $this->jobSpecializationService->all($request->all());
        $jobSpecializationResult = $dataJobSpecializations->getData()->result;
        if ($jobSpecializationResult != null) {
            $jobSpecializations = $jobSpecializationResult->data;
        }

        $dataJobRoles = $this->jobRoleService->all($request->all());
        $jobRoleResult = $dataJobRoles->getData()->result;
        if ($jobRoleResult != null) {
            $jobRoles = $jobRoleResult->data;
        }

        $dataSkills = $this->skillService->all($request->all());
        $skillResult = $dataSkills->getData()->result;
        if ($skillResult != null) {
            $skills = $skillResult->data;
        }

        $dataWorkLocations = $this->workLocationService->all($request->all());
        $workLocationResult = $dataWorkLocations->getData()->result;
        if ($workLocationResult != null) {
            $workLocations = $workLocationResult->data;
        }

        $dataWorkPRefenreces = $this->workPreferenceService->all($request->all());
        $workPreferenceResult = $dataWorkPRefenreces->getData()->result;
        if ($workPreferenceResult != null) {
            $workPreferences = $workPreferenceResult->data;
        }

        $this->params['job_types'] = $jobTypes;
        $this->params['companies'] = $companies;
        $this->params['career_levels'] = $careerLevels;
        $this->params['job_specializations'] = $jobSpecializations;
        $this->params['job_roles'] = $jobRoles;
        $this->params['skills'] = $skills;
        $this->params['work_locations'] = $workLocations;
        $this->params['work_preferences'] = $workPreferences;
        $this->params['selected_job_specializations'] = [];
        $this->params['selected_job_roles'] = [];
        $this->params['selected_skills'] = [];
        $this->params['selected_job_specializations'] = [];
        $this->params['selected_work_locations'] = [];
        $this->params['selected_work_preferences'] = [];

        return parent::add($request);
    }

    public function store(JobStoreRequest $request)
    {
        $data = $request->all();

        // Data Slug
        $now = Carbon::now();
        $now = $now->timestamp;
        $data['slug'] = $now . '-' . strtolower(str_replace(" ", "-", $data['title']));

        // Job Specialization
        $arrJobSpecializations = $data['job_specialization'];
        $labels = $this->jobSpecializationService->masterUpdateOrCreate($arrJobSpecializations);
        $data['job_specialization'] = implode(', ', $labels);

        // Work Location
        $arrWorkLocations = $data['work_location'];
        $labels = $this->workLocationService->masterUpdateOrCreate($arrWorkLocations);
        $data['work_location'] = implode(', ', $labels);

        $storeData = $this->service->create($data);
        return $this->storeProcess($storeData);
    }

    public function detail(Request $request, $id)
    {
        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $this->params['job'] = $dataDetail;

        return parent::detail($request, $id);
    }

    public function edit(Request $request, $id)
    {
        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $jobTypes = [];
        $companies = [];
        $careerLevels = [];
        $jobSpecializations = [];
        $selectedJobSpecializations = [];
        $workLocations = [];
        $selectedWorkLocations = [];

        $dataJobTypes = $this->jobTypeService->all($request->all());
        $jobTypeResult = $dataJobTypes->getData()->result;
        if ($jobTypeResult != null) {
            $jobTypes = $jobTypeResult->data;
        }

        $dataCompanies = $this->companyService->all($request->all());
        $companyResult = $dataCompanies->getData()->result;
        if ($companyResult != null) {
            $companies = $companyResult->data;
        }

        $dataCareerLevels = $this->careerLevelService->all($request->all());
        $careerLevelResult = $dataCareerLevels->getData()->result;
        if ($careerLevelResult != null) {
            $careerLevels = $careerLevelResult->data;
        }

        $dataJobSpecializations = $this->jobSpecializationService->all($request->all());
        $jobSpecializationResult = $dataJobSpecializations->getData()->result;
        if ($jobSpecializationResult != null) {
            $jobSpecializations = $jobSpecializationResult->data;
        }

        $dataWorkLocations = $this->workLocationService->all($request->all());
        $workLocationResult = $dataWorkLocations->getData()->result;
        if ($workLocationResult != null) {
            $workLocations = $workLocationResult->data;
        }

        $this->params['job_types'] = $jobTypes;
        $this->params['companies'] = $companies;
        $this->params['career_levels'] = $careerLevels;
        $this->params['job_specializations'] = $jobSpecializations;
        $this->params['work_locations'] = $workLocations;

        $this->params['job'] = $dataDetail;

        $selectedJobSpecializations = explode(', ', $dataDetail->job_specialization);
        $selectedWorkLocations = explode(', ', $dataDetail->work_location);

        $this->params['selected_job_specializations'] = $selectedJobSpecializations;
        $this->params['selected_work_locations'] = $selectedWorkLocations;

        return parent::edit($request, $id);
    }

    public function update(JobUpdateRequest $request, $id)
    {
        $data = $request->all();

        // Job Specialization
        $arrJobSpecializations = $data['job_specialization'];
        $labels = $this->jobSpecializationService->masterUpdateOrCreate($arrJobSpecializations);
        $data['job_specialization'] = implode(', ', $labels);

        // Work Location
        $arrWorkLocations = $data['work_location'];
        $labels = $this->workLocationService->masterUpdateOrCreate($arrWorkLocations);
        $data['work_location'] = implode(', ', $labels);

        $updatedData = $this->service->update($data, $id);
        return $this->updateProcess($updatedData);
    }
}
