<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\JobRoleService;
use App\Services\Admin\WorkPreferenceService;
use Illuminate\Http\Request;

class WorkPreferenceController extends MasterGeneralController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(WorkPreferenceService $service)
    {
        $this->tableName = 'work_preferences';

        parent::__construct();

        $this->service = $service;

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'Work Preference';

        $this->viewIndex = 'content.admin.work-preference.index';
        $this->viewDetail = 'content.admin.work-preference.form';
        $this->viewForm = 'content.admin.work-preference.form';

        $this->routeDetail = 'admin-work-preference-detail';
        
        $this->variableDetail = 'workPreference';
    }
}
