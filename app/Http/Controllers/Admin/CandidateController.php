<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CandidateStoreRequest;
use App\Http\Requests\Admin\CompanyStoreRequest;
use App\Http\Requests\Admin\CompanyUpdateRequest;
use App\Services\Admin\CandidateService;
use App\Services\Admin\CompanyAttributeService;
use App\Services\Admin\DegreeService;
use App\Services\Admin\JobRoleService;
use App\Services\Admin\JobSpecializationService;
use App\Services\Admin\SkillService;
use App\Services\Admin\WorkPreferenceService;
use PDF;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $workPreferenceService;
    protected $skillService;
    protected $degreeService;
    protected $jobSpecializationService;
    protected $jobRoleService;

    public function __construct(CandidateService $service, WorkPreferenceService $workPreferenceService, SkillService $skillService, DegreeService $degreeService, JobSpecializationService $jobSpecializationService, JobRoleService $jobRoleService)
    {
        parent::__construct();

        $this->service = $service;

        $this->workPreferenceService = $workPreferenceService;
        $this->skillService = $skillService;
        $this->degreeService = $degreeService;
        $this->jobSpecializationService = $jobSpecializationService;
        $this->jobRoleService = $jobRoleService;

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'Candidate';

        $this->uploadFolder = 'candidate';

        $this->viewIndex = 'content.admin.candidate.index';
        $this->viewDetail = 'content.admin.candidate.detail';
        $this->viewForm = 'content.admin.candidate.form';

        $this->routeDetail = 'admin-candidate-detail';
    }

    public function add(Request $request)
    {
        $workPreferences = [];
        $skills = [];
        $degrees = [];
        $jobSpecializations = [];
        $jobRoles = [];

        $dataWorkPreferences = $this->workPreferenceService->all($request->all());
        $workPreferenceResult = $dataWorkPreferences->getData()->result;
        if ($workPreferenceResult != null) {
            $workPreferences = $workPreferenceResult->data;
        }

        $dataSkills = $this->skillService->all($request->all());
        $skillResult = $dataSkills->getData()->result;
        if ($skillResult != null) {
            $skills = $skillResult->data;
        }

        $dataDegrees = $this->degreeService->all($request->all());
        $degreeResult = $dataDegrees->getData()->result;
        if ($degreeResult != null) {
            $degrees = $degreeResult->data;
        }

        $dataJobSpecializations = $this->jobSpecializationService->all($request->all());
        $jobSpecializationResult = $dataJobSpecializations->getData()->result;
        if ($jobSpecializationResult != null) {
            $jobSpecializations = $jobSpecializationResult->data;
        }

        $dataJobRoles = $this->jobRoleService->all($request->all());
        $jobRoleResult = $dataJobRoles->getData()->result;
        if ($jobRoleResult != null) {
            $jobRoles = $jobRoleResult->data;
        }

        $this->params['work_preferences'] = $workPreferences;
        $this->params['skills'] = $skills;
        $this->params['degrees'] = $degrees;
        $this->params['job_specializations'] = $jobSpecializations;
        $this->params['job_roles'] = $jobRoles;

        return parent::add($request);
    }


    public function store(CandidateStoreRequest $request)
    {
        $filename = 'candidate_image_' . date('YmdHi');
        $data = $this->uploadFile($request->all(), 'photo_profile', true, $filename);

        $cvFilename = 'cv_candidate_' . date('YmdHi');
        $data = $this->uploadFile($data, 'cv', true, $cvFilename);

        if (isset($request->is_completed)) {
            if ($request->is_completed == 'on') {
                $data['is_completed'] = true;
            } else {
                $data['is_completed'] = false;
            }
        }

        $workPreferenceIds = isset($request->work_preference_id) ? $request->work_preference_id : [];

        $data['work_preference_id'] = implode(",", $workPreferenceIds);

        $storeData = $this->service->create($data);
        return $this->storeProcess($storeData);
    }

    public function detail(Request $request, $id)
    {
        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $this->params['candidate'] = $dataDetail;

        return parent::detail($request, $id);
    }

    public function downloadPdf(Request $request, $id)
    {
        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $this->params['candidate'] = $dataDetail;
        $filename = "CV-" . str_replace(" ", "-", ucwords($dataDetail->name)) . ".pdf";

        // return view('content.admin.candidate.detail-pdf', $this->params);
        // return view('tes', $this->params);

        // $pdf = PDF::loadview('tes', $this->params);
        $pdf = PDF::loadview('content.admin.candidate.detail-pdf', $this->params);
        // $pdf->setWatermarkImage(public_path('assets/images/smiley.png'));
        return $pdf->download($filename);
        // return parent::detail($request, $id);
    }

    public function edit(Request $request, $id)
    {
        $workPreferences = [];
        $skills = [];
        $degrees = [];
        $jobSpecializations = [];
        $jobRoles = [];

        $dataWorkPreferences = $this->workPreferenceService->all($request->all());
        $workPreferenceResult = $dataWorkPreferences->getData()->result;
        if ($workPreferenceResult != null) {
            $workPreferences = $workPreferenceResult->data;
        }

        $dataSkills = $this->skillService->all($request->all());
        $skillResult = $dataSkills->getData()->result;
        if ($skillResult != null) {
            $skills = $skillResult->data;
        }

        $dataDegrees = $this->degreeService->all($request->all());
        $degreeResult = $dataDegrees->getData()->result;
        if ($degreeResult != null) {
            $degrees = $degreeResult->data;
        }

        $this->params['work_preferences'] = $workPreferences;
        $this->params['skills'] = $skills;
        $this->params['degrees'] = $degrees;

        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $selectedSkills = [];
        $candidateskills = $dataDetail->skills;
        foreach ($candidateskills as $candidateSkill) {
            array_push($selectedSkills, $candidateSkill->skill_id);
        }

        $selectedWorkPreferences = [];
        $candidateWorkPreferences = explode(",", $dataDetail->work_preference_id);
        foreach ($candidateWorkPreferences as $workPreference) {
            array_push($selectedWorkPreferences, $workPreference);
        }

        $dataJobSpecializations = $this->jobSpecializationService->all($request->all());
        $jobSpecializationResult = $dataJobSpecializations->getData()->result;
        if ($jobSpecializationResult != null) {
            $jobSpecializations = $jobSpecializationResult->data;
        }

        $dataJobRoles = $this->jobRoleService->all($request->all());
        $jobRoleResult = $dataJobRoles->getData()->result;
        if ($jobRoleResult != null) {
            $jobRoles = $jobRoleResult->data;
        }

        $this->params['selected_skills'] = $selectedSkills;
        $this->params['candidate'] = $dataDetail;
        $this->params['job_specializations'] = $jobSpecializations;
        $this->params['job_roles'] = $jobRoles;

        $this->params['selected_work_preference'] = $selectedWorkPreferences;
        $this->params['selected_job_specializations'] = [];
        $this->params['selected_job_roles'] = [];

        // dd($this->params['candidate']);

        return parent::edit($request, $id);
    }

    public function update(Request $request, $id)
    {
        $filename = 'candidate_image_' . date('YmdHi');
        $data = $this->uploadFile($request->all(), 'photo_profile', true, $filename);

        $cvFilename = 'cv_candidate_' . date('YmdHi');
        $data = $this->uploadFile($data, 'cv', true, $cvFilename);

        if (isset($request->is_completed)) {
            if ($request->is_completed == 'on') {
                $data['is_completed'] = true;
            } else {
                $data['is_completed'] = false;
            }
        }

        $workPreferenceIds = isset($request->work_preference_id) ? $request->work_preference_id : [];

        $data['work_preference_id'] = implode(",", $workPreferenceIds);

        $updatedData = $this->service->update($data, $id);
        return $this->updateProcess($updatedData);
    }

    public function sendEmail(Request $request, $id)
    {
        return $this->service->sendEmail($id);
    }
}
