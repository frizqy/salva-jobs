<?php

namespace App\Http\Controllers\Admin;

use App\Services\Admin\JobTypeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class JobTypeController extends MasterGeneralController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(JobTypeService $service)
    {
        $this->tableName = 'job_types';

        parent::__construct();

        $this->service = $service;

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'Job Types';

        $this->viewIndex = 'content.admin.job-type.index';
        $this->viewDetail = 'content.admin.job-type.form';
        $this->viewForm = 'content.admin.job-type.form';

        $this->routeDetail = 'admin-job-type-detail';

        $this->variableDetail = 'jobType';
    }
}
