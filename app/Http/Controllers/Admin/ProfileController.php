<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ChangePasswordRequest;
use App\Models\User;
use App\Services\Admin\JobTitleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $jobTitleService;

    public function __construct(JobTitleService $jobTitleService)
    {
        parent::__construct();

        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'User Profile';

        $this->uploadFolder = 'user';

        $this->viewIndex = 'content.admin.profile.index';
        $this->viewForm = 'content.admin.profile.form';

        $this->routeDetail = 'admin-profile-detail';

        $this->jobTitleService = $jobTitleService;
    }

    public function index(Request $request)
    {
        $jobTitles = [];
        $dataJobTitles = $this->jobTitleService->all($request->all());
        $jobTitleResult = $dataJobTitles->getData()->result;
        if ($jobTitleResult != null) {
            $jobTitles = $jobTitleResult->data;
        }

        $this->params['job_titles'] = $jobTitles;

        $user = Auth::user();
        $this->params['user'] = $user;
        return view($this->viewIndex, $this->params);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $id = $user->id;
        $filename = 'user_image_' . date('YmdHi');
        $data = $this->uploadFile($request->all(), 'photo_profile', true, $filename);

        $object = User::findOrFail($id);
        $object->fill($data);
        $object->save();

        return redirect()->back()->with('success', __('action.update_success'));
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = Auth::user();

        if (!(Hash::check($request->get('current_password'), $user->password))) {
            return redirect()->back()->with("error", "Your current password does not matches with the password.");
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            return redirect()->back()->with("error", "New Password cannot be same as your current password.");
        }

        $object = User::findOrFail($user->id);
        $object->password = bcrypt($request->new_password);
        $object->save();

        return redirect()->back()->with('success', __('action.update_success'));
    }
}
