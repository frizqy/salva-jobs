<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MasterGeneralController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $variableDetail = 'detail';
    protected $ruleStore = [];
    protected $ruleUpdate = [];
    protected $customMessages = [];
    protected $customAttributes = [];

    protected $tableName = "";

    public function __construct()
    {
        parent::__construct();

        $this->ruleStore = [
            'label' => ['required'],
            'code' => ['required', 'unique:' . $this->tableName . ',code'],
        ];

        $this->ruleUpdate = [
            'label' => ['required'],
            'code' => ['required']
        ];

        $this->customAttributes = [
            'label' => 'Name'
        ];
    }

    public function index(Request $request)
    {
        return parent::index($request);
    }

    // BASE VIEW FORM ADD
    public function add(Request $request)
    {
        return parent::add($request);
    }

    // PROCESS STORE
    public function store(Request $request)
    {
        $data = $request->all();
        $label = $data['label'];
        $code = str_replace([',', '-', ' ', '.'], ['_', '_', '_', '_'], strtolower($label));
        $data['code'] = $code;

        $this->validator($data, $this->ruleStore)->validate();
        $storeData = $this->service->create($data);
        return $this->storeProcess($storeData);
    }

    // BASE VIEW DETAIL
    public function detail(Request $request, $id)
    {
        $this->params['action'] = 'edit';

        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $this->params[$this->variableDetail] = $dataDetail;

        return parent::detail($request, $id);
    }

    // BASE VIEW FORM EDIT
    public function edit(Request $request, $id)
    {
        $this->params['action'] = 'edit';

        $detail = $this->service->detail($id);
        $result = $detail->getData()->result;
        $dataDetail = $result->data;

        $this->params[$this->variableDetail] = $dataDetail;

        return parent::edit($request, $id);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        if ($this->tableName != "") {
            array_push($this->ruleUpdate['code'], 'unique:' . $this->tableName . ',code,' . $id);
        }

        $this->validator($data, $this->ruleUpdate)->validate();

        $updatedData = $this->service->update($data, $id);
        return $this->updateProcess($updatedData);
    }

    protected function validator(array $data, $rules)
    {
        return Validator::make($data, $rules, $this->customMessages, $this->customAttributes);
    }
}
