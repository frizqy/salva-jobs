<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CareerLevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->params['layout_wrapper'] = "compact-wrapper";
        $this->params['breadcrumbs_holder'] = false;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index(Request $request)
    // {
    //     return view('content.admin.job-type.index', $this->params);
    // }

    // public function detail(Request $request, $id)
    // {
    //     return view('content.admin.job-type.index', $this->params);
    // }
}
