<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $uploadFolder = '';
    public $params;

    public $mainModel;
    public $service;

    public $viewIndex;
    public $viewDetail;
    public $viewForm;

    public $routeIndex;
    public $routeDetail;
    public $routeAdd;
    public $routeUpdate;

    public $storeReequest;
    public $updateRequest;

    public function __construct()
    {
        $this->params['layout_wrapper'] = 'compact-wrapper';
        $this->params['breadcrumbs_holder'] = false;
        $this->params['title'] = 'Jobs';
    }

    // BASE VIEW INDEX
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->getDatatables($request);
        }

        return view($this->viewIndex, $this->params);
    }

    // BASE VIEW FORM ADD
    public function add(Request $request)
    {
        $this->params['action'] = 'add';
        return view($this->viewForm, $this->params);
    }

    // PROCESS STORE
    public function storeProcess($storedData)
    {
        $result = $storedData->getData()->result;
        if ($result != null) {
            $storedData = $result->data;
            return redirect()->route($this->routeDetail, ['id' => $storedData->id])->with('success', __('action.create_success'));
        }

        return redirect()->back()->with('error', __('action.create_failed'));
    }


    // BASE VIEW DETAIL
    public function detail(Request $request, $id)
    {
        return view($this->viewDetail, $this->params);
    }

    // BASE VIEW FORM EDIT
    public function edit(Request $request, $id)
    {
        $this->params['action'] = 'edit';
        return view($this->viewForm, $this->params);
    }

    // PROCESS UPDATE
    public function updateProcess($updatedData)
    {
        $result = $updatedData->getData()->result;
        if ($result != null) {
            $updatedData = $result->data;
            return redirect()->route($this->routeDetail, ['id' => $updatedData->id])->with('success', __('action.update_success'));
        }

        return redirect()->back()->with('error', __('action.update_failed'));
    }

    // =============================================================================================================================================================================
    // =============================================================================================================================================================================

    public function dataTableFormatter($data)
    {
        return datatables()
            ->collection($data->data)
            ->filter(function () {
            })
            ->setTotalRecords($data->meta->total)
            ->setFilteredRecords($data->meta->total)
            ->skipPaging()
            ->make(true);
    }

    public function uploadFile(array $data, $key, $rename = false, $filename = "")
    {
        if (isset($data[$key])) {
            $file = $data[$key];
            $filepath = 'public/' . $this->uploadFolder;
            $extension = $file->extension();

            $uploadedFile = Storage::put($filepath, $file);
            // $filename = basename($uploadedFile);
            if ($rename && $filename !== "") {
                $newFilename = $filepath . '/' . $filename . '.' . $extension;
                if (Storage::exists($newFilename)) {
                    Storage::delete($newFilename);
                }
                Storage::move($uploadedFile, $newFilename);
            } else {
                $newFilename = $uploadedFile;
            }
            $data[$key] = $newFilename;
        }

        return $data;
    }

    public function getDatatables(Request $request)
    {
        $request['dt'] = true;
        $request = $this->setRequestParam($request);

        $datas = $this->service->all($request->all());
        return $this->dataTableFormatter($datas->getData()->result);
    }

    public function setRequestParam(Request $request)
    {
        if (array_key_exists('dt', $request->all())) {
            $limit = (int) $request['length'];
            $page     = ($limit != 0) ? ceil($request['start'] / $limit) + 1 : 1;
            if (array_key_exists('search', $request->all())) {
                if (array_key_exists('value', $request['search'])) {
                    $q = $request['search']['value'];
                    $request['q'] = $q;
                }
            }

            $request['page'] = $page;
            $request['limit'] = $limit;
        }

        Arr::except($request, 'dt');

        return $request;
    }
}
