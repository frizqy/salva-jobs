<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Company;
use App\Models\Admin\Job;
use App\Repositories\BaseRepository;

class JobRepository extends BaseRepository
{
    public function __construct(Job $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}