<?php

namespace App\Repositories\Admin;

use App\Models\Admin\CandidateCertification;
use App\Repositories\BaseRepository;

class CandidateCertificationRepository extends BaseRepository
{
    public function __construct(CandidateCertification $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/

    public function deleteWhereNotIn($ids, $id)
    {
        $object = $this->model->whereNotIn('id', $ids)->where('candidate_id', $id);
        return $object->delete();
    }
}