<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Degree;
use App\Repositories\BaseRepository;

class DegreeRepository extends BaseRepository
{
    public function __construct(Degree $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}