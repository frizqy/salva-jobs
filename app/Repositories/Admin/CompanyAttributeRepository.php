<?php

namespace App\Repositories\Admin;

use App\Models\Admin\CompanyAttribute;
use App\Repositories\BaseRepository;

class CompanyAttributeRepository extends BaseRepository
{
    public function __construct(CompanyAttribute $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}