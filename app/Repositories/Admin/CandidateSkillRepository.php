<?php

namespace App\Repositories\Admin;

use App\Models\Admin\CandidateSkill;
use App\Repositories\BaseRepository;

class CandidateSkillRepository extends BaseRepository
{
    public function __construct(CandidateSkill $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/

    public function destroyByCandidate($candidateId)
    {
        $object = $this->model->where('candidate_id', $candidateId);
        return $object->delete();
    }
}