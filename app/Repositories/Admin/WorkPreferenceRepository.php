<?php

namespace App\Repositories\Admin;

use App\Models\Admin\WorkPreference;
use App\Repositories\BaseRepository;

class WorkPreferenceRepository extends BaseRepository
{
    public function __construct(WorkPreference $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}