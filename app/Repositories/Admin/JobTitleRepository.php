<?php

namespace App\Repositories\Admin;

use App\Models\Admin\JobTitle;
use App\Repositories\BaseRepository;

class JobTitleRepository extends BaseRepository
{
    public function __construct(JobTitle $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}