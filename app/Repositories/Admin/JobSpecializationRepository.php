<?php

namespace App\Repositories\Admin;

use App\Models\Admin\JobSpecialization;
use App\Repositories\BaseRepository;

class JobSpecializationRepository extends BaseRepository
{
    public function __construct(JobSpecialization $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}