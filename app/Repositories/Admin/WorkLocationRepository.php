<?php

namespace App\Repositories\Admin;

use App\Models\Admin\WorkLocation;
use App\Repositories\BaseRepository;

class WorkLocationRepository extends BaseRepository
{
    public function __construct(WorkLocation $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}