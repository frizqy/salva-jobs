<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Candidate;
use App\Repositories\BaseRepository;

class CandidateRepository extends BaseRepository
{
    public function __construct(Candidate $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}