<?php

namespace App\Repositories\Admin;

use App\Models\Admin\CareerLevel;
use App\Models\Admin\Company;
use App\Models\Admin\Job;
use App\Models\Admin\JobType;
use App\Repositories\BaseRepository;

class CareerLevelRepository extends BaseRepository
{
    public function __construct(CareerLevel $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}