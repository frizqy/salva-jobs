<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Company;
use App\Repositories\BaseRepository;

class CompanyRepository extends BaseRepository
{
    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}