<?php

namespace App\Repositories\Admin;

use App\Models\Admin\JobRole;
use App\Models\Admin\JobSpecialization;
use App\Repositories\BaseRepository;

class JobRoleRepository extends BaseRepository
{
    public function __construct(JobRole $model)
    {
        $this->model = $model;
    }

    /*
    * Create your custom function here
    * ...
    **/
}