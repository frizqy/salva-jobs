<?php

namespace App\Repositories;

interface RepositoryInterface
{
    /**
     * Retrieve all data
     *
     * @param       $query
     * @param       $request
     * @param       $filter
     *
     * @return collection
     */
    public function all($request, $filter);

    /**
     * Retrieve data find by id
     *
     * @param       $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * Retrieve data find by field and value
     *
     * @param string $field
     * @param string $value
     *
     * @return mixed
     */
    public function findByField($field, $value);

    /**
     * Retrieve data find by multiple field
     *
     * @param array $where
     *
     * @return mixed
     */
    public function findWhere(array $where);

    /**
     * Retrieve data find by multiple values in one field
     *
     * @param string $field
     * @param array $values
     *
     * @return mixed
     */
    public function findWhereIn($field, array $values);

    /**
     * Retrieve data find by multiple values not in one field
     *
     * @param string $field
     * @param array $values
     *
     * @return mixed
     */
    public function findWhereNotIn($field, array $values);

    /**
     * Create new data
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * Create multiple new data based on relationship
     *
     * @param       $relation
     * @param array $attributes
     *
     * @return mixed
     */
    public function createMany($relation, array $attributes);

    /**
     * Update an existing data find by id
     *
     * @param array $attributes
     * @param       $id
     *
     * @return mixed
     */
    public function update(array $attributes, $id);

    /**
     * Destroy an existing data find by id
     *
     * @param array $attributes
     * @param       $id
     *
     * @return mixed
     */
    public function destroy($id);

    /**
     * Destroy an existing data find by multiple field
     *
     * @param array $where
     *
     * @return mixed
     */
    public function destroyWhere(array $where);

    /**
     * Load relations
     *
     * @param array $relations
     *
     * @return this
     */
    public function with(array $relations);
}
