<?php

namespace App\Repositories;

use App\Filters\BaseFilter;
use App\Helpers\Datatable;
use App\Helpers\Pagination;
use Illuminate\Database\Eloquent\Relations\Relation;

class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function setBaseData($query, $request, $filter)
    {
        $request = Datatable::handle($request);
        $query = BaseFilter::apply($query, $request, $filter);
        $datas = Pagination::paginate($query, $request);
        if (array_key_exists('dt', $request) && $request['dt'] === 'true') {
            return datatables()
                ->collection($datas['data'])
                ->filter(function () {
                })
                ->setTotalRecords($datas['meta']['total'])
                ->setFilteredRecords($datas['meta']['total'])
                ->skipPaging()
                ->make(true);
        }
        return $datas;
    }

    public function all($request, $filter)
    { 
        $query = $this->model;
        $datas = $this->setBaseData($query, $request, $filter);
        return $datas;
    }

    public function allInactive($request, $filter)
    {
        $query = $this->model->onlyTrashed();
        $datas = $this->setBaseData($query, $request, $filter);
        return $datas;
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function findByField($field, $value)
    {
        return $this->model->where($field, '=', $value)->get();
    }

    public function findWhere(array $where)
    {
        $data = $this->model->where(function ($q) use ($where) {
            foreach ($where as $column => $value) {
                $q->where($column, $value);
            }
        })->get();

        return $data;
    }

    public function findWhereIn($field, array $values)
    {
        return $this->model->whereIn($field, $values)->get();
    }

    public function findWhereNotIn($field, array $values)
    {
        return $this->model->whereNotIn($field, $values)->get();
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function createOne($relation, array $attributes)
    {
        return $relation->create($attributes);
    }

    public function createMany($relation, array $attributes)
    {
        return $relation->createMany($attributes);
    }

    public function update(array $attributes, $id)
    {
        $object = $this->model->findOrFail($id);
        $object->fill($attributes);
        $object->save();
        return $object->fresh();
    }

    public function updateOrCreate(array $keys, array $attributes)
    {
        return $this->model->updateOrCreate($keys, $attributes);
    }

    public function destroy($id)
    {
        $object = $this->model->findOrFail($id);
        return $object->delete();
    }

    public function destroyWhere(array $where)
    {
        $data = $this->model::where(function ($q) use ($where) {
            foreach ($where as $column => $value) {
                $q->where($column, $value);
            }
        });

        return $data->delete();
    }

    public function with($relations)
    {
        $this->model = $this->model->with($relations);
        return $this;
    }

    public function orderBy($field, $order = 'asc')
    {
        $this->model = $this->model->orderBy($field, $order);
        return $this;
    }

    public function has($relations)
    {
        $this->model = $this->model->has($relations);
        return $this;
    }

    public function insert(array $data)
    {
        return $this->model::insert($data);
    }

    public function whereRaw($strRaw){
        $this->model = $this->model->whereRaw($strRaw);
        return $this;
    }
}
