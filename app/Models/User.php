<?php

namespace App\Models;

use App\Models\Admin\Company;
use App\Models\Admin\JobTitle;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'is_super_admin',
        'company_id',
        'photo_profile',
        'job_title_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static $logName = 'User';
    protected static $logFillable = true;

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function job_title()
    {
        return $this->belongsTo(JobTitle::class);
    }

    public function getPhotoProfileAttribute($value)
    {
        if ($value != null) {
            $value = str_replace("public", "storage", $value);
        } else {
            $value = 'assets/images/default/avatar.png';
        }
        return $value;
    }
}
