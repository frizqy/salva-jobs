<?php

namespace App\Models\Admin;

use App\Scopes\AuthorizedManagerJobScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Job extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'jobs';

    protected $fillable = [
        'company_id',
        'job_type_id',
        'title',
        'job_description',
        'requirement',
        'benefit',
        'qualification',
        'year_experience',
        'min_sallary',
        'max_sallary',
        'job_specialization',
        'work_location',
        'career_level_id',
        'open_until',
        'slug',
        'description',
    ];

    protected static $logName = 'Job';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new AuthorizedManagerJobScope);
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('jobs.id', 'desc');
        });
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
    
    public function job_type()
    {
        return $this->belongsTo(JobType::class);
    }

    public function career_level()
    {
        return $this->belongsTo(CareerLevel::class);
    }
}
