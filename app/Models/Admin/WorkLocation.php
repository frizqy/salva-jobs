<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class WorkLocation extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'work_locations';

    protected $fillable = [
        'code',
        'label'
    ];

    protected static $logName = 'Work Location';
    protected static $logFillable = true;
}
