<?php

namespace App\Models\Admin;

use App\Scopes\AuthorizedManagerCompanyScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Candidate extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'candidates';

    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'birthday_location',
        'birthday',
        'gender',
        'current_sallary',
        'expected_sallary',
        'current_sallary_type',
        'expected_sallary_type',
        'reason_to_leave',
        'notice_period',
        'work_preference_id',
        'is_completed',
        'cv',
        'photo_profile',
        'instagram',
        'facebook',
        'linkedin',
        'job_specialization_id',
        'job_role_id',
    ];

    protected static $logName = 'Candidate';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
    }

    public function educations()
    {
        return $this->hasMany(CandidateEducation::class);
    }

    public function skills()
    {
        return $this->hasMany(CandidateSkill::class);
    }

    public function work_experiences()
    {
        return $this->hasMany(CandidateWorkExperience::class);
    }

    public function certifications()
    {
        return $this->hasMany(CandidateCertification::class);
    }

    public function languages()
    {
        return $this->hasMany(CandidateLanguage::class);
    }

    public function job_specialization()
    {
        return $this->belongsTo(JobSpecialization::class);
    }

    public function job_role()
    {
        return $this->belongsTo(JobRole::class);
    }

    // public function work_preference()
    // {
    //     return $this->belongsTo(WorkPreference::class);
    // }


    public function getPhotoProfileAttribute($value)
    {
        if ($value != null) {
            $value = str_replace("public", "storage", $value);
        } else {
            $value = 'assets/images/default/avatar.png';
        }
        return $value;
    }

    public function getWorkPreferenceAttribute()
    {
        $workPreferences = [];

        $workPreferenceIds = explode(",", $this->work_preference_id);
        $workPreferences = WorkPreference::whereIn('id', $workPreferenceIds)->get();

        return $workPreferences;
    }

    public function getWorkPreferenceLabelAttribute()
    {
        $workPreferences = [];

        $workPreferenceIds = explode(",", $this->work_preference_id);
        $workPreferences = WorkPreference::whereIn('id', $workPreferenceIds)->pluck('label')->toArray();

        return implode(", ", $workPreferences);
    }

    protected $appends = ['work_preference', 'work_preference_label'];
}
