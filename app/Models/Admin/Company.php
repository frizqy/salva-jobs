<?php

namespace App\Models\Admin;

use App\Scopes\AuthorizedManagerCompanyScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Company extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'companies';

    protected $fillable = [
        'name',
        'address',
        'image',
        'description',
        'employee_size_id',
        'industry_id',
        'benefit',
        'instagram',
        'facebook',
        'linkedin',
        'website',
        'phone_number'
    ];

    protected static $logName = 'Company';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new AuthorizedManagerCompanyScope);
    }

    public function getImageAttribute($value)
    {
        if ($value != null) {
            $value = str_replace("public", "storage", $value);
        } else {
            $value = 'assets/images/default/default.png';
        }
        return $value;
    }

    public function employee_size()
    {
        return $this->belongsTo(CompanyAttribute::class, 'employee_size_id');
    }

    public function industry()
    {
        return $this->belongsTo(CompanyAttribute::class, 'industry_id');
    }
}
