<?php

namespace App\Models\Admin;

use App\Scopes\AuthorizedManagerCompanyScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CandidateLanguage extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'candidate_languages';

    protected $fillable = [
        'candidate_id',
        'language',
        'writing',
        'speaking',
        'listening'
    ];

    protected static $logName = 'Candidate Language';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
    }
}
