<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CompanyAttribute extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'company_attributes';

    protected $fillable = [
        'group',
        'code',
        'label'
    ];

    protected static $logName = 'Job Type';
    protected static $logFillable = true;
}
