<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class JobRole extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'job_roles';

    protected $fillable = [
        'code',
        'label',
        'job_specialization_id',
    ];

    protected static $logName = 'Job Role';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('job_roles.job_specialization_id', 'asc');
        });
    }

    public function job_specialization()
    {
        return $this->belongsTo(JobSpecialization::class, 'job_specialization_id', 'id');
    }
}
