<?php

namespace App\Models\Admin;

use App\Scopes\AuthorizedManagerCompanyScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CandidateSkill extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'candidate_skills';

    protected $fillable = [
        'candidate_id',
        'skill_id',
        'level',
        'rating',
    ];

    protected static $logName = 'Candidate Skill';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
    }

    public function skill()
    {
        return $this->belongsTo(Skill::class);
    }
}
