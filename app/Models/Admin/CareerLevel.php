<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CareerLevel extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'career_levels';

    protected $fillable = [
        'code',
        'label'
    ];

    protected static $logName = 'Career Level';
    protected static $logFillable = true;
}
