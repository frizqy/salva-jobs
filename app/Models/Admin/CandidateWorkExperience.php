<?php

namespace App\Models\Admin;

use App\Scopes\AuthorizedManagerCompanyScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CandidateWorkExperience extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'candidate_work_experiences';

    protected $fillable = [
        'candidate_id',
        'company',
        'date_from',
        'date_to',
        'job_description',
        'used_technology',
        'job_title',
    ];

    protected static $logName = 'Candidate Work Experiences';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
    }

    public function getSkillAttribute()
    {
        $exploded = explode(", ", $this->used_technology);
        return $exploded;
    }

    public function getPresentAttribute()
    {
        if ($this->date_to != null && $this->date_to == 'present') {
            return 'present';
        }

        return '';
    }

    protected $appends = ['skill', 'present'];
}
