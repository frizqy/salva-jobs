<?php

namespace App\Models\Admin;

use App\Scopes\AuthorizedManagerCompanyScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CandidateCertification extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'candidate_certifications';

    protected $fillable = [
        'candidate_id',
        'name',
        'organization',
        'year',
        'event_description'
    ];

    protected static $logName = 'Candidate Certification';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
    }

    public function getEventNameAttribute()
    {
        return $this->name;
    }

    public function getEventYearAttribute()
    {
        return $this->year;
    }

    protected $appends = ['event_name', 'event_year'];
}
