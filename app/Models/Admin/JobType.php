<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class JobType extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'job_types';

    protected $fillable = [
        'code',
        'label'
    ];

    protected static $logName = 'Job Type';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('job_types.id', 'asc');
        });
    }
}
