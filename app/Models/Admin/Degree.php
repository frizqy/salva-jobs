<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Degree extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'degrees';

    protected $fillable = [
        'code',
        'label'
    ];

    protected static $logName = 'Degree';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('degrees.id', 'asc');
        });
    }
}
