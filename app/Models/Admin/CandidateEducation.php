<?php

namespace App\Models\Admin;

use App\Scopes\AuthorizedManagerCompanyScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CandidateEducation extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = 'candidate_educations';

    protected $fillable = [
        'candidate_id',
        'name',
        'degree',
        'date_from',
        'date_to',
        'gpa',
        'specialization',
    ];

    protected static $logName = 'Candidate Education';
    protected static $logFillable = true;

    protected static function boot()
    {
        parent::boot();
    }

    public function getEducationNameAttribute()
    {
        return $this->name;
    }

    public function getEducationDegreeAttribute()
    {
        return $this->degree;
    }

    protected $appends = ['education_name', 'education_degree'];
}
