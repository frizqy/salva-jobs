<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCandidateCertificationAddColumnYearRemoveSomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidate_certifications', function (Blueprint $table) {
            $table->integer('year');
            $table->dropColumn(['location']);
            $table->dropColumn(['date_from']);
            $table->dropColumn(['date_to']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidate_certifications', function (Blueprint $table) {
            $table->dropColumn(['year']);
            $table->string('location')->nullable();
            $table->string('date_from')->nullable();
            $table->string('date_to')->nullable();
        });
    }
}
