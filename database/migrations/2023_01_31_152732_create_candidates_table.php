<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->bigIncrements("id");

            $table->string('name');
            $table->string('email')->unique();

            $table->string('phone_number')->nullable();
            $table->string('birthday_location')->nullable();
            $table->date('birthday')->nullable();
            $table->string('gender', 50)->nullable();

            $table->bigInteger('current_sallary')->default(0);
            $table->bigInteger('expected_sallary')->default(0);
            $table->text('reason_to_leave')->nullable();
            $table->string('notice_period')->nullable();
            $table->bigInteger('work_preference_id')->nullable();
            $table->boolean('is_info_fulfilled')->default(false);

            $table->text('cv');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
