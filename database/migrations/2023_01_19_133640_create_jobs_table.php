<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements("id");

            $table->bigInteger('company_id')->unsigned()->index();
            $table->bigInteger('job_type_id')->unsigned()->index();

            $table->string('title', 200)->index();
            $table->text('job_description');
            $table->text('requirement');
            $table->text('benefit');

            $table->text('qualification')->default('Not Specified');
            $table->bigInteger('year_experience');

            $table->bigInteger('min_sallary')->index()->default(0);
            $table->bigInteger('max_sallary')->index()->default(0);

            $table->text('job_specialization')->index();
            $table->text('work_location')->index();

            $table->text('career_level')->nullable();

            $table->date('open_until');

            $table->date('slug');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
