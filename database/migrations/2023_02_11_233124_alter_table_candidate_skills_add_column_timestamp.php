<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCandidateSkillsAddColumnTimestamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidate_skills', function (Blueprint $table) {
            $table->timestamps();
            $table->softDeletes();
            $table->dropColumn(['used_technology']);
            $table->text('level')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidate_skills', function (Blueprint $table) {
            $table->dropColumn(['created_at']);
            $table->dropColumn(['updated_at']);
            $table->dropColumn(['deleted_at']);
            $table->text('used_technology')->nullable();
            $table->text('level')->nullable(false)->change();
        });
    }
}
