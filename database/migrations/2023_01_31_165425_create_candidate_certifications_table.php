<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidateCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_certifications', function (Blueprint $table) {
            $table->bigIncrements("id");

            $table->bigInteger("candidate_id")->unsigned();
            $table->string('name');
            $table->string('organization')->nullable();
            $table->string('location')->nullable();
            $table->string('date_from');
            $table->string('date_to');
            $table->text('event_description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_certifications');
    }
}
