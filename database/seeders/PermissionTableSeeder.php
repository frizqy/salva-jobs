<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $datas = [
            ['name' => 'manage_job'],
            ['name' => 'view_job'],
            ['name' => 'create_job'],
            ['name' => 'update_job'],
            ['name' => 'delete_job'],

            ['name' => 'manage_company'],
            ['name' => 'view_company'],
            ['name' => 'create_company'],
            ['name' => 'update_company'],
            ['name' => 'delete_company'],

            ['name' => 'apply_jobs'],
        ];

        foreach ($datas as $data) {
            Permission::updateOrCreate(['name' => $data['name']], ['name' => $data['name'], 'guard_name' => 'web']);
        }
    }
}
