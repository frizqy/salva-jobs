<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $datas = [
            [
                'name' => 'super_admin', 'guard_name' => 'web', 'permissions' => []
            ],
            [
                'name' => 'company_admin', 'guard_name' => 'web', 'permissions' => [
                    'manage_job',
                    'view_job',
                    'create_job',
                    'update_job',
                    'delete_job',

                    'manage_company',
                    'view_company',
                    'update_company',
                ]
            ],
            ['name' => 'administrator', 'guard_name' => 'web', 'permissions' => []],
            [
                'name' => 'candidate', 'guard_name' => 'web', 'permissions' => [
                    'view_job',
                    'apply_jobs',
                ]
            ],
        ];

        foreach ($datas as $data) {
            $permissions = $data['permissions'];
            $data = Arr::except($data, 'permissions');

            $role = Role::updateOrCreate(['name' => $data['name']], $data);

            foreach ($permissions as $permission) {
                $role->givePermissionTo($permission);
            }
        }
    }
}
