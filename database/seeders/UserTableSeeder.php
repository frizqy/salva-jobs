<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userSuperAdmin = User::updateOrCreate(
            ['email' => 'sysadmin@mail.com'],
            [
                'first_name' => 'System',
                'last_name' => 'Administrator',
                'email' => 'sysadmin@mail.com',
                'password' => bcrypt(config('ap.user.sysadmin.password')),
                'is_super_admin' => true
            ]
        );

        $roleSuperAdmin = Role::where('name', 'super_admin')->get()->first();
        $userSuperAdmin->assignRole($roleSuperAdmin);
    }
}
