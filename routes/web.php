<?php

use App\Http\Controllers\Admin\CandidateController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\DegreeController;
use App\Http\Controllers\Admin\JobController;
use App\Http\Controllers\Admin\JobRoleController;
use App\Http\Controllers\Admin\JobSpecializationController;
use App\Http\Controllers\Admin\JobTypeController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\SearchCvController;
use App\Http\Controllers\Admin\SkillController;
use App\Http\Controllers\Admin\WorkPreferenceController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// Auth::routes();
// Authentication Routes...
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

// Registration Routes...
Route::prefix('/employer')->group(function () {
    Route::get('/register', [RegisterController::class, 'showRegistrationEmployerForm'])->name('register-employer');
    Route::post('register', [RegisterController::class, 'registerEmployer']);
});
Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('register', [RegisterController::class, 'register']);

// Password Reset Routes...
Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm']);
Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm']);
Route::post('password/reset', [ResetPasswordController::class, 'reset']);

Route::get('/storage-link', function () {
        Artisan::call('storage:link');
        Artisan::call('optimize:clear');
        Artisan::call('config:clear');
});

Route::middleware(['auth'])->group(function () {

    Route::get('/dt-job-roles', [JobRoleController::class, 'index'])->name('dt-job-roles');

    Route::prefix('/job-search')->group(function () {
        Route::get('/', [HomeController::class, 'index'])->name('index');
        Route::get('/{slug}', [HomeController::class, 'detail'])->name('detail');
        Route::get('/{id}/apply', [HomeController::class, 'apply'])->name('job-apply');
    });

    Route::prefix('/profile')->group(function () {
        Route::get('/', [ProfileController::class, 'index'])->name('profile');
        Route::post('/change-password', [ProfileController::class, 'changePassword'])->name('change-password');
        Route::post('/', [ProfileController::class, 'update'])->name('update-profile');
    });

    Route::name('admin-')->group(function () {
        Route::prefix('/jobs')->name('job-')->group(function () {
            Route::get('/', [JobController::class, 'index'])->name('index');
            Route::middleware('permission:create_job')->get('/add', [JobController::class, 'add'])->name('add');
            Route::middleware('permission:create_job')->post('/', [JobController::class, 'store'])->name('store');
            Route::get('/{id}', [JobController::class, 'detail'])->name('detail');
            Route::middleware('permission:update_job')->get('/{id}/edit', [JobController::class, 'edit'])->name('edit');
            Route::middleware('permission:update_job')->post('/{id}', [JobController::class, 'update'])->name('update');
        });

        Route::prefix('/companies')->name('company-')->group(function () {
            Route::get('/', [CompanyController::class, 'index'])->name('index');
            // Route::middleware('permission:create_company')->get('/add', [CompanyController::class, 'add'])->name('add');
            Route::middleware('permission:create_company')->post('/', [CompanyController::class, 'store'])->name('store');
            Route::get('/{id}', [CompanyController::class, 'detail'])->name('detail');
            Route::middleware('permission:update_company')->get('/{id}/edit', [CompanyController::class, 'edit'])->name('edit');
            Route::middleware('permission:update_company')->post('/{id}', [CompanyController::class, 'update'])->name('update');
        });

        Route::prefix('/search-cvs')->name('search-cv-')->group(function () {
            Route::get('/', [SearchCvController::class, 'index'])->name('index');
        });

        Route::middleware('role:super_admin')->group(function () {
            Route::prefix('/candidates')->name('candidate-')->group(function () {
                Route::get('/', [CandidateController::class, 'index'])->name('index');
                Route::get('/add', [CandidateController::class, 'add'])->name('add');
                Route::post('/', [CandidateController::class, 'store'])->name('store');
                Route::get('/{id}', [CandidateController::class, 'detail'])->name('detail');
                Route::get('/{id}/pdf', [CandidateController::class, 'downloadPdf'])->name('pdf');
                Route::get('/{id}/send-email', [CandidateController::class, 'sendEmail'])->name('invitation-email');
                Route::get('/{id}/edit', [CandidateController::class, 'edit'])->name('edit');
                Route::post('/{id}', [CandidateController::class, 'update'])->name('update');
            });


            Route::prefix('/job-types')->name('job-type-')->group(function () {
                Route::get('/', [JobTypeController::class, 'index'])->name('index');
                Route::get('/add', [JobTypeController::class, 'add'])->name('add');
                Route::post('/', [JobTypeController::class, 'store'])->name('store');
                Route::get('/{id}', [JobTypeController::class, 'detail'])->name('detail');
                Route::get('/{id}/edit', [JobTypeController::class, 'edit'])->name('edit');
                Route::post('/{id}', [JobTypeController::class, 'update'])->name('update');
            });

            Route::prefix('/job-specializations')->name('job-specialization-')->group(function () {
                Route::get('/', [JobSpecializationController::class, 'index'])->name('index');
                Route::get('/add', [JobSpecializationController::class, 'add'])->name('add');
                Route::post('/', [JobSpecializationController::class, 'store'])->name('store');
                Route::get('/{id}', [JobSpecializationController::class, 'detail'])->name('detail');
                Route::get('/{id}/edit', [JobSpecializationController::class, 'edit'])->name('edit');
                Route::post('/{id}', [JobSpecializationController::class, 'update'])->name('update');
            });

            Route::prefix('/job-roles')->name('job-role-')->group(function () {
                Route::get('/', [JobRoleController::class, 'index'])->name('index');
                Route::get('/add', [JobRoleController::class, 'add'])->name('add');
                Route::post('/', [JobRoleController::class, 'store'])->name('store');
                Route::get('/{id}', [JobRoleController::class, 'detail'])->name('detail');
                Route::get('/{id}/edit', [JobRoleController::class, 'edit'])->name('edit');
                Route::post('/{id}', [JobRoleController::class, 'update'])->name('update');
            });

            Route::prefix('/work-preferences')->name('work-preference-')->group(function () {
                Route::get('/', [WorkPreferenceController::class, 'index'])->name('index');
                Route::get('/add', [WorkPreferenceController::class, 'add'])->name('add');
                Route::post('/', [WorkPreferenceController::class, 'store'])->name('store');
                Route::get('/{id}', [WorkPreferenceController::class, 'detail'])->name('detail');
                Route::get('/{id}/edit', [WorkPreferenceController::class, 'edit'])->name('edit');
                Route::post('/{id}', [WorkPreferenceController::class, 'update'])->name('update');
            });

            Route::prefix('/skills')->name('skill-')->group(function () {
                Route::get('/', [SkillController::class, 'index'])->name('index');
                Route::get('/add', [SkillController::class, 'add'])->name('add');
                Route::post('/', [SkillController::class, 'store'])->name('store');
                Route::get('/{id}', [SkillController::class, 'detail'])->name('detail');
                Route::get('/{id}/edit', [SkillController::class, 'edit'])->name('edit');
                Route::post('/{id}', [SkillController::class, 'update'])->name('update');
            });

            Route::prefix('/degrees')->name('degree-')->group(function () {
                Route::get('/', [DegreeController::class, 'index'])->name('index');
                Route::get('/add', [DegreeController::class, 'add'])->name('add');
                Route::post('/', [DegreeController::class, 'store'])->name('store');
                Route::get('/{id}', [DegreeController::class, 'detail'])->name('detail');
                Route::get('/{id}/edit', [DegreeController::class, 'edit'])->name('edit');
                Route::post('/{id}', [DegreeController::class, 'update'])->name('update');
            });

        });
    });
});

Route::get('/', [HomeController::class, 'index'])->name('job-list');
Route::get('/job/{slug}', [HomeController::class, 'detail'])->name('job-detail');
Route::get('/{id}/apply', [HomeController::class, 'apply'])->name('job-apply');
